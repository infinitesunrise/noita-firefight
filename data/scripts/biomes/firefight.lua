dofile_once("data/scripts/biome_scripts.lua")
dofile_once("mods/noita-firefight/files/scripts/utils.lua")

RegisterSpawnFunction(0xff00ff11, "load_random_shop_item")

function load_random_shop_item(x, y)
    --don't spawn item if player's item inventory is full
    local items = 0
    local player_id = EntityGetWithTag("player_unit")[1]
    if player_id == nil then return end
    local children = EntityGetAllChildren(player_id)
    for _, child in ipairs(children) do
        if EntityGetName(child) == "inventory_quick" then
            local children2 = EntityGetAllChildren(child)
            for _, child2 in ipairs(children2) do
                local reloader_comp = EntityGetComponent(child2, "ManaReloaderComponent")
                if reloader_comp == nil then
                    items = items + 1
                end
            end
        end
    end
    if items >= 4 then return end

    --spawn shop dummy item
    local dummy_item = EntityLoad(
        "mods/noita-firefight/files/entities/dummy_shop_item/dummy_shop_item.xml",
        x, y)

    --roll for actual item
	local time = GameGetRealWorldTimeSinceStarted()
    local frame = GameGetDateAndTimeUTC()
    SetRandomSeed(tonumber(y + time), tonumber(x + frame))
    local item_name = Items[Random(1, #Items)]
    local item_path = tostring("data/entities/items/pickup/" .. item_name .. ".xml")

    EntityAddComponent(dummy_item, "VariableStorageComponent", {
        name = "item_path",
        value_string = item_path
    })
    
    local actual_item = EntityLoad(item_path)

    local actual_sprite_comp = EntityGetFirstComponentIncludingDisabled(actual_item, "SpriteComponent")
    if actual_sprite_comp ~= nil then
        local dummy_sprite = EntityGetFirstComponentIncludingDisabled(dummy_item, "SpriteComponent")
        ComponentSetValue2(dummy_sprite, "image_file", ComponentGetValue2(actual_sprite_comp, "image_file"))
        ComponentSetValue2(dummy_sprite, "offset_x", ComponentGetValue2(actual_sprite_comp, "offset_x"))
        ComponentSetValue2(dummy_sprite, "offset_y", ComponentGetValue2(actual_sprite_comp, "offset_y"))
    end

    local actual_item_comp = EntityGetFirstComponentIncludingDisabled(actual_item, "ItemComponent")
    if actual_item_comp ~= nil then
        local dummy_item_comp = EntityGetFirstComponentIncludingDisabled(dummy_item, "ItemComponent")
        ComponentSetValue2(dummy_item_comp, "item_name", ComponentGetValue2(actual_item_comp, "item_name"))
    end

    local actual_ui_comp = EntityGetFirstComponentIncludingDisabled(actual_item, "UIInfoComponent")
    if actual_ui_comp ~= nil then
        local dummy_ui_comp = EntityGetFirstComponentIncludingDisabled(dummy_item, "UIInfoComponent")
        ComponentSetValue2(dummy_ui_comp, "name", ComponentGetValue2(actual_ui_comp, "name"))
    end

    local actual_emitter_comps = EntityGetComponentIncludingDisabled(actual_item, "ParticleEmitterComponent")
    if actual_emitter_comps ~= nil then
        for _, emitter_comp in ipairs(actual_emitter_comps) do
            copy_component_to_entity(emitter_comp, dummy_item)
        end
    end

    local actual_image_emitter_comps = EntityGetComponentIncludingDisabled(actual_item, "SpriteParticleEmitterComponent")
    if actual_image_emitter_comps ~= nil then
        for _, image_emitter_comp in ipairs(actual_image_emitter_comps) do
            copy_component_to_entity(image_emitter_comp, dummy_item)
        end
    end

    local actual_light_comps = EntityGetComponentIncludingDisabled(actual_item, "LightComponent")
    if actual_light_comps ~= nil then
        for _, light_comp in ipairs(actual_light_comps) do
            copy_component_to_entity(light_comp, dummy_item)
        end
    end

    --calculate current cost
    local item_cost = 50
    if GlobalsGetValue("noita-firefight.player_spawned", "0") == "1" then
        dofile_once("mods/noita-firefight/files/scripts/store.lua")
        item_cost = 50 + ((FIREFIGHT.round - 1) * 10)
    end

    --update cost component
    local cost_component = EntityGetFirstComponentIncludingDisabled(dummy_item, "ItemCostComponent")
    if cost_component ~= nil then
        ComponentSetValue2(cost_component, "cost", tonumber(item_cost))
    end

    --update cost text sprite
    local sprite_comps = EntityGetComponentIncludingDisabled(dummy_item, "SpriteComponent")
    for _, sprite_comp in ipairs(sprite_comps) do
        if ComponentGetValue2(sprite_comp, "is_text_sprite") then
            ComponentSetValue2(sprite_comp, "text", tostring(item_cost))
            local new_offset_x = (#tostring(item_cost) * 3) - 2
            ComponentSetValue2(sprite_comp, "offset_x", new_offset_x)
        end
    end

    EntityKill(actual_item)
end