dofile_once("mods/noita-firefight/files/scripts/fancy_pixels.lua")
dofile_once("mods/noita-firefight/files/scripts/utils.lua")
dofile_once("mods/noita-together/files/scripts/utils.lua")

ModTextFileSetContent("mods/noita-together/files/ws/ws.lua", ModTextFileGetContent("mods/noita-firefight/files/scripts/nt_ws_appends.lua"))
ModTextFileSetContent("mods/noita-together/files/scripts/playerghost.lua", ModTextFileGetContent("mods/noita-firefight/files/scripts/nt_playerghost_appends.lua"))
ModTextFileSetContent("mods/noita-together/files/scripts/ui.lua", ModTextFileGetContent("mods/noita-firefight/files/scripts/nt_ui_appends.lua"))
ModLuaFileAppend("mods/noita-together/files/ws/events.lua", "mods/noita-firefight/files/scripts/nt_events_appends.lua")
ModLuaFileAppend("mods/noita-together/init.lua", "mods/noita-firefight/files/scripts/nt_init_appends.lua")
ModLuaFileAppend("mods/noita-together/files/scripts/utils.lua", "mods/noita-firefight/files/scripts/nt_utils_appends.lua")
ModLuaFileAppend("data/scripts/gun/procedural/gun_procedural.lua", "mods/noita-firefight/files/scripts/gun_procedural_appends.lua")
ModRegisterAudioEventMappings("mods/noita-firefight/files/audio/audio_events.txt")
ModMaterialsFileAdd("mods/noita-firefight/files/misc/materials.xml")

version = "2023051400"
fight_length = 90
tinker_length = 30
countdown = 4
last_clock_display = nil
last_board_message = nil
last_board_routine = nil
reloads = 1
--wand_chance = 5
--spell_chance = 20
base_enemy_spawn_delay = 120
firefight_bounds = {
    min_x = -512,
    max_x = 512,
    min_y = -512,
    max_y = 0
}
echo_wait = 0
max_echo_wait = 180
player_x, player_y = nil, nil
real_time = 0
frame_num = 0
last_unpoly_frame = 0

--mangle NT's RequestGameInfo responder to include Firefight version in returned mod IDs
local nt_events_content = ModTextFileGetContent("mods/noita-together/files/ws/events.lua")
nt_events_content = nt_events_content:gsub(
    "local mods = ModGetActiveModIDs%(%)",
    "local mods = ModGetActiveModIDs() for index, mod in pairs(mods) do if mod == \"noita-firefight\" then mods[index] = \"noita-firefight v" .. tostring(version) .. "\" end end"
)
ModTextFileSetContent("mods/noita-together/files/ws/events.lua", nt_events_content)

--add mod biomes
local nxml = dofile_once("mods/noita-firefight/nxml.lua")
local _biomes_all = ModTextFileGetContent("data/biome/_biomes_all.xml")
local xml = nxml.parse(_biomes_all)
xml:add_children(
    nxml.parse_many([[
        <Biome 
            biome_filename="data/biome/bounds.xml" 
            height_index="10"
            color="ff000001" />
        <Biome 
            biome_filename="data/biome/firefight.xml" 
            height_index="10"
            color="ff00ff00" />
    ]])
)
ModTextFileSetContent("data/biome/_biomes_all.xml", tostring(xml))

function OnModInit()
    --maagisia numeroita
    local mn_biome_map = 'BIOME_MAP="mods/noita-firefight/data/biome_impl/biome_map.png"'
    local mn_spawn_x = 'DESIGN_PLAYER_START_POS_X="0"'
    local mn_spawn_y = 'DESIGN_PLAYER_START_POS_Y="-212"'
    ModTextFileSetContent(
        "mods/noita-firefight/magic_numbers_appends.xml",
        "<MagicNumbers " .. mn_biome_map .. " " .. mn_spawn_x .. " " .. mn_spawn_y .. " />"
    )
    ModMagicNumbersFileAdd("mods/noita-firefight/magic_numbers_appends.xml")
end

function OnPlayerSpawned(player_id)
    dofile_once("mods/noita-firefight/files/scripts/store.lua")

    --go hard, wake up the neighbors
    EntityAddTag(player_id, "music_energy_100")

    --double spell inventory size
    local inventory_comp = EntityGetFirstComponentIncludingDisabled(player_id, "Inventory2Component")
    if inventory_comp ~= nil then
        ComponentSetValue2(inventory_comp, "full_inventory_slots_y", 2)
    end

    --add the combo system entity for combo anouncements
    local combo_system = EntityLoad("mods/noita-firefight/files/entities/combo_system/combo_system.xml", player_x, player_y)
    EntityAddChild(player_id, combo_system)

    reset_firefight()
    randomize_start_position(player_id)
    add_area_constraint(player_id, "firefight_constraint", -512, 512, -512, 0)
    load_firefight_entities()
    initialize_left_statue()
    announce_self()

    if FIREFIGHT.phase == "pregame" then
        set_board_clock("0000")
        set_board_message("waiting for host to start the game... ", LEDRoutine.SCROLL)
        add_area_constraint(player_id, "start_area_constraint", -101, 101, -258, -211)
    end
end

function OnWorldPreUpdate()
    dofile_once("mods/noita-firefight/files/scripts/store.lua")

    --if we don't know who we are yet, ask other players
    if FIREFIGHT.userId == nil then
        if echo_wait > 1 then
            echo_wait = echo_wait - 1
        else
            send_echo_request()
            echo_wait = max_echo_wait
        end
    end
    
    real_time = GameGetRealWorldTimeSinceStarted()
    frame_num = GameGetFrameNum()
    local player_id = EntityGetWithTag("player_unit")[1]
    if player_id ~= nil then
        player_x, player_y = EntityGetTransform(player_id)
    end

    if frame_num % 300 == 0 
    and FIREFIGHT.phase ~= "fight"
    and FIREFIGHT.phase ~= "pregame" then
        reset_firefight()
    end

    --PREGAME
    if FIREFIGHT.phase == "pregame" then
        --keep protections applied
        if player_id ~= nil then
            local has_protection = false
            local children = EntityGetAllChildren(player_id)
            for _, child in ipairs(children) do
                local game_effect_comps = EntityGetComponentIncludingDisabled(child, "GameEffectComponent")
                if game_effect_comps ~= nil then
                    for _, game_effect_comp in ipairs(game_effect_comps) do
                        local effect = ComponentGetValue2(game_effect_comp, "effect")
                        if effect == "PROTECTION_POLYMORPH"
                        or effect == "PROTECTION_ALL" then
                            has_protection = true
                        end
                    end
                end
            end
            if has_protection == false then
                print("adding protection")
                reset_status_effect_component(player_id)
                toggle_status_effect_component(player_id, false)
                wash_stains(player_id)
                set_iframes(player_id, 7200)
            end
        end

    --COUNTDOWN
    elseif FIREFIGHT.phase == "countdown" then
        local countdown_now = (3 - math.floor(real_time - FIREFIGHT.last_phase_start))
        if countdown_now ~= countdown then
            countdown = countdown_now
            set_board_message(tostring(countdown_now), LEDRoutine.STATIC)
            set_board_clock(tostring(countdown_now))
            clock_sound_effect("countdown_tone")
        end

        --setup fight phase
        if countdown_now <= 0 then

            local player_x, player_y = EntityGetTransform(player_id)
            GameTriggerMusicEvent("music/miniboss/00", false, player_x, player_y)

            FIREFIGHT.phase = "fight"
            FIREFIGHT.last_phase_start = real_time
            countdown = 4

            reset_status_effect_component(player_id)
            toggle_status_effect_component(player_id, true)
            wash_stains(player_id)
            set_iframes(player_id, 0)
            remove_area_constraint(player_id, "start_area_constraint")
            start_area_force_field("off")
            clock_sound_effect("start_tone")
            set_board_message("GO GO GO GO GO GO GO GO GO", LEDRoutine.SCROLL)
            initialize_left_statue()
            load_item_shop()
        end

    --FIGHT
    elseif FIREFIGHT.phase == "fight" then
        if real_time - FIREFIGHT.last_phase_start > 3 then
            set_board_message("ROUND " .. tostring(FIREFIGHT.round), LEDRoutine.LIST)
        end
        local time_remaining = fight_length - math.floor(real_time - FIREFIGHT.last_phase_start)
        set_board_clock(seconds_to_clock_string(time_remaining))

        --3 frames less spawn delay each round, down to 60
        if frame_num % math.max(60, base_enemy_spawn_delay - (FIREFIGHT.round * 3)) == 0 then
            --rolls for enemy level
            local level = 1 + math.floor(FIREFIGHT.round / 3)
            local level_boost_roll = Random(100)
            if level_boost_roll < 2 then
                level = level + 2
            elseif level_boost_roll < 12 then
                level = level + 1
            end
            --rolls for enemy spawn
            --local player_x, player_y = EntityGetTransform(player_id)
            local nearby_enemies = EntityGetInRadiusWithTag(player_x, player_y, 128, "enemy")
            local spawn_x, spawn_y = 0, 0
            if #nearby_enemies < 2 then
                local in_bounds = false
                while not in_bounds do
                    spawn_x, spawn_y = random_point_near(player_x, player_y, 20, 128)
                    if spawn_x > firefight_bounds.min_x
                    and spawn_x < firefight_bounds.max_x
                    and spawn_y > firefight_bounds.min_y
                    and spawn_y < firefight_bounds.max_y then
                        in_bounds = true
                    end
                end
            else
                spawn_x = Random(384, 512)
                if Random() > 0.5 then
                    spawn_x = -spawn_x
                end
                spawn_y = Random(-512, 0)
            end
            spawn_enemy(spawn_x, spawn_y, level)
        end

        --setup tinker phase
        if real_time - FIREFIGHT.last_phase_start > fight_length then
            FIREFIGHT.phase = "tinker"
            FIREFIGHT.last_phase_start = real_time

            set_board_message(
                "tinker time! get ready for round " .. tostring(FIREFIGHT.round + 1) .. " ",
                LEDRoutine.SCROLL)

            --turn on the start area force field
            start_area_force_field("on")

            --kill all enemies + drops, and clean firefight
            clean_firefight_entities()
            reset_firefight()

            --spawn the border repair entity
            EntityLoad("mods/noita-firefight/files/entities/border_repair/border_repair.xml")

            play_tinker_music()

            --undo polymorphs in case player is a sheep
            if player_id == nil then
                undo_polymorphs()
            else
                --teleport back to start area and constrain
                randomize_start_position(player_id)
                add_area_constraint(player_id, "start_area_constraint", -101, 101, -258, -211)

                --give tinker
                grant_tinker(player_id)

                --reset status effects
                reset_status_effect_component(player_id)
                toggle_status_effect_component(player_id, false)
                wash_stains(player_id)

                --invincible
                set_iframes(player_id, 9999)

                --re-up
                restore_health_and_abilities(player_id)

                GameTriggerMusicFadeOutAndDequeueAll(1)
            end
        end

    --TINKER
    elseif FIREFIGHT.phase == "tinker" then
        --modify player 2 frames later if we had to unpoly them during phase transition
        if last_unpoly_frame == frame_num - 2 then
            --teleport back to start area and constrain
            randomize_start_position(player_id)
            add_area_constraint(player_id, "start_area_constraint", -101, 101, -258, -211)

            --give tinker
            grant_tinker(player_id)

            --reset status effects
            reset_status_effect_component(player_id)
            toggle_status_effect_component(player_id, false)
            wash_stains(player_id)

            --invincible
            set_iframes(player_id, 9999)

            --re-up
            restore_health_and_abilities(player_id)
        end

        --remove harmful or exploitable game effects
        if player_id ~= nil then
            local children = EntityGetAllChildren(player_id)
            for _, child in ipairs(children) do
                local game_effect_comp = EntityGetFirstComponentIncludingDisabled(child, "GameEffectComponent")
                if game_effect_comp ~= nil then
                    local effect = ComponentGetValue2(game_effect_comp, "effect")
                    if effect ~= nil then
                        if effect  == "POLYMORPH"
                        or effect == "POISON"
                        or effect == "ON_FIRE"
                        or effect == "FROZEN"
                        or effect == "RADIOACTIVE"
                        or effect == "FOOD_POISONING" then
                            EntityKill(child)
                        end
                    end
                end
            end
        end

        local time_remaining = tinker_length - math.floor(real_time - FIREFIGHT.last_phase_start)

        if time_remaining < 0 then
            if frame_num % 60 == 0 then
                FIREFIGHT.ready = true
                send_countdown_ready()
            end
        end

        if time_remaining < 0 then
            set_board_clock("0000")
            set_board_message("waiting for all game clients to be ready... ", LEDRoutine.SCROLL)

            --setup countdown phase
            if everyone_is_ready_for_countdown() then
                FIREFIGHT.phase = "countdown"
                FIREFIGHT.last_phase_start = real_time
                FIREFIGHT.round = FIREFIGHT.round + 1

                --reset countdown readiness for next round
                local player_list = json.decode(FIREFIGHT.PlayerList)
                for k, player in pairs(player_list) do
                    player.ready = false
                end
                FIREFIGHT.PlayerList = json.encode(player_list)
                FIREFIGHT.ready = false

                --reset round_sends on each player
                FIREFIGHT.round_sends = 0
                local player_list = json.decode(FIREFIGHT.PlayerList)
                for k, player in pairs(player_list) do
                    player.round_sends = 0
                end
                FIREFIGHT.PlayerList = json.encode(player_list)

                local round_base_send_cost = round_base_send_cost()
                local statue_left_entities = EntityGetWithTag("statue_left")
                for _, entity in ipairs(statue_left_entities) do
                    if EntityGetName(entity) == "statue_left_purchase" then
                        local purchase_sprite_component = EntityGetFirstComponent(entity, "SpriteComponent") 
                        if purchase_sprite_component ~= nil then
                            ComponentSetValue2(purchase_sprite_component, "text", tostring(round_base_send_cost))
                        end
                        local item_cost_comp = EntityGetFirstComponent(entity, "ItemCostComponent")
                        if item_cost_comp ~= nil then
                            ComponentSetValue2(item_cost_comp, "cost", round_base_send_cost)
                        end
                    end
                end

                --clean firefight again
                reset_firefight()

                --remove tinker
                revoke_tinker(player_id)

                play_fight_music()
            end
        else
            set_board_clock(seconds_to_clock_string(time_remaining))
        end

    --POST
    elseif FIREFIGHT.phase == "post" then
        if frame_num % 120 == 0 then
            clean_firefight_enemies()
        end
    end
end
