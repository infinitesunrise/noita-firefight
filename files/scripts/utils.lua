--led board routines
LEDRoutine = {
    OFF = 0,    --display is off
    SCROLL = 1, --right to left scroll
    BLINK = 2,  --on and off static centered flashing
    LIST = 3,   --flash one line at a time, left aigned, | symbol is line break
    STATIC = 4  --single unchanging message, centered
}

EnemyTiers = {}
EnemyTiers[1] = {
    "longleg",
    "longleg",
    "longleg",
    "rat",
    "rat",
    "rat",
    "zombie_weak",
    "zombie_weak",
    "zombie_weak",
    "miner_weak",
    "miner_weak",
    "shotgunner_weak",
    "slimeshooter_weak"
}
EnemyTiers[2] = {
    "miner_weak",
    "shotgunner_weak",
    "slimeshooter_weak",
    "bat",
    "shotgunner",
    "frog",
    "miner",
    "miner_chef",
    "miner_santa",
    "miniblob",
    "zombie",
    "acidshooter_weak",
    "firebug",
    "fireskull",
    "goblin_bomb",
    "zombie"
}
EnemyTiers[3] = {
    "acidshooter",
    "iceskull",
    "firemage_weak",
    "fungus",
    "fly",
    "bigfirebug",
    "blob",
    "drone_lasership",
    "bigbat",
    "frog_big",
    "miner_fire",
    "scavenger_grenade",
    "scavenger_smg",
    "scavenger_heal",
    "scavenger_invis",
    "shaman",
    "slimeshooter",
    "sniper",
    "tank",
    "tentacler_small",
    "worm"
}
EnemyTiers[4] = {
    "ant",
    "alchemist",
    "barfer",
    "bloom",
    "shooterflower",
    "coward",
    "drone",
    "fungus_big",
    "fungus_giga",
    "fungus_tiny",
    "giant",
    "giantshooter",
    "lasershooter",
    "maggot",
    "monk",
    "phantom_a",
    "roboguard",
    "scavenger_clusterbomb",
    "scavenger_glue",
    "scavenger_leader",
    "scavenger_mine",
    "skullrat",
    "tank_rocket",
    "tentacler",
    "thundermage",
    "wizard_dark",
    "wizard_returner",
    "wizard_swapper",
    "wizard_tele",
    "wizard_twitchy",
    "wizard_weaken",
    "wizard_wither",
    "wolf",
    "worm_big"
}
EnemyTiers[5] = {
    "assassin",
    "bigzombie",
    "chest_mimic",
    "crystal",
    "enlightened_alchemist",
    "failed_alchemist",
    "healerdrone",
    "flamer",
    "icer",
    "missilecrab",
    "necromancer",
    "phantom_b",
    "scavenger_poison",
    "scavenger_shield",
    "skullfly",
    "skullmage",
    "spearbot",
    "statue_physics",
    "tank_super",
    "thunderskull",
    "wizard_hearty",
    "wizard_neutral",
    "wizard_poly",
    "wraith"
}
EnemyTiers[6] = {
    "basebot_hidden",
    "basebot_neutralizer",
    "basebot_sentry",
    "basebot_soldier",
    "necrobot",
    "necrobot_super",
    "gazer",
    "skygazer",
    "spitmonster",
    "lurker",
    "roboguard_big"
}
EnemyTiers[7] = {
    "necromancer_shop"
}

SpellTiers = {}
SpellTiers[1] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 21, 22, 27, 28, 29, 32, 33, 34, 35, 36, 38, 41, 
    42, 69, 70, 82, 83, 84, 89, 152, 157, 160, 161, 162, 163, 212, 234, 262, 263, 264, 266, 317, 318, 319, 320, 321, 
    341, 342, 333, 334 }
SpellTiers[2] = { 20, 23, 24, 37, 39, 43, 45, 47, 48, 49, 50, 62, 64, 65, 66, 71, 73, 74, 85, 87, 92, 94, 95, 97, 98, 
    99, 102, 106, 110, 119, 120, 121, 122, 123, 124, 125, 126, 136, 137, 138, 139, 140, 153, 158, 166, 167, 176, 184, 
    185, 186, 191, 192, 193, 194, 201, 199, 204, 208, 217, 218, 221, 222, 223, 228, 237, 241, 242, 243, 244, 245, 246, 
    247, 248, 249, 265, 267, 270, 271, 273, 274, 275, 277, 280, 282, 283, 284, 285, 302, 303, 308, 309, 310, 311, 360, 
    361, 362, 371 }
SpellTiers[3] = { 25, 26, 30, 40, 44, 46, 51, 52, 53, 54, 55, 56, 57, 58, 59, 67, 68, 72, 77, 78, 79, 80, 86, 88, 93, 
    100, 103, 107, 108, 112, 113, 114, 115, 116, 117, 118, 131, 132, 133, 134, 135, 154, 159, 164, 168, 172, 173, 174, 
    175, 180, 181, 187, 188, 189, 190, 195, 196, 197, 198, 200, 202, 203, 207, 209, 213, 219, 235, 236, 238, 239, 240, 
    256, 257, 258, 259, 260, 261, 269, 276, 278, 281, 286, 288, 289, 292, 293, 294, 295, 296, 298, 299, 300, 301, 305, 
    315, 324, 325, 326, 327, 345, 346, 347, 355, 356, 357, 358, 363, 364, 366, 385, 386, 387, 388, 389, 390, 391, 392, 
    393 }
SpellTiers[4] = { 31, 60, 63, 75, 76, 81, 90, 101, 104, 105, 111, 127, 128, 129, 130, 141, 143, 151, 155, 165, 169, 170, 
    171, 177, 178, 179, 182, 183, 205, 206, 210, 211, 214, 215, 216, 220, 224, 225, 226, 227, 229, 232, 250, 251, 252, 
    253, 254, 255, 279, 287, 290, 291, 297, 304, 306, 307, 316, 350, 351, 352, 353, 359, 365, 367, 368, 369, 370, 372, 378 }
SpellTiers[5] = { 91, 142, 144, 156, 314, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 348, 349, 373, 
    374, 375, 379, 380, 381, 382, 383, 384 }

Perks = { "CRITICAL_HIT","BREATH_UNDERWATER","EXTRA_MONEY","EXTRA_MONEY_TRICK_KILL",
"HOVER_BOOST","FASTER_LEVITATION","MOVEMENT_FASTER",
"STRONG_KICK","TELEKINESIS","REPELLING_CAPE","EXPLODING_CORPSES","SAVING_GRACE","INVISIBILITY",
"GLOBAL_GORE","LEVITATION_TRAIL","VAMPIRISM","EXTRA_HP","GLASS_CANNON","LOW_HP_DAMAGE_BOOST",
"RESPAWN","RADAR_ENEMY","FOOD_CLOCK","IRON_STOMACH","PROTECTION_FIRE","PROTECTION_RADIOACTIVITY",
"PROTECTION_EXPLOSION","PROTECTION_MELEE","PROTECTION_ELECTRICITY","TELEPORTITIS",
"TELEPORTITIS_DODGE","STAINLESS_ARMOUR","EDIT_WANDS_EVERYWHERE","ABILITY_ACTIONS_MATERIALIZED",
"PROJECTILE_HOMING","PROJECTILE_HOMING_SHOOTER","UNLIMITED_SPELLS","FREEZE_FIELD","FIRE_GAS",
"DISSOLVE_POWDERS","BLEED_SLIME","BLEED_OIL","BLEED_GAS","SHIELD","REVENGE_EXPLOSION",
"REVENGE_TENTACLE","REVENGE_RATS","REVENGE_BULLET","ATTACK_FOOT","LEGGY_FEET","PLAGUE_RATS",
"VOMIT_RATS","CORDYCEPS","MOLD","PROJECTILE_REPULSION","RISKY_CRITICAL","FUNGAL_DISEASE",
"PROJECTILE_SLOW_FIELD","PROJECTILE_REPULSION_SECTOR","PROJECTILE_EATER_SECTOR","ORBIT",
"ANGRY_GHOST","HUNGRY_GHOST","DEATH_GHOST","HOMUNCULUS","LUKKI_MINION","ELECTRICITY",
"EXTRA_KNOCKBACK","LOWER_SPREAD","LOW_RECOIL","BOUNCE","FAST_PROJECTILES",
"ALWAYS_CAST","EXTRA_MANA","NO_MORE_SHUFFLE","NO_MORE_KNOCKBACK","DUPLICATE_PROJECTILE",
"FASTER_WANDS","EXTRA_SLOTS","CONTACT_DAMAGE","GAMBLE","MANA_FROM_KILLS","ANGRY_LEVITATION",
"LASER_AIM","PERSONAL_LASER","MEGA_BEAM_STONE" }

Items = {
    "brimstone",
    "broken_wand",
    "egg_fire",
    "egg_monster",
    "egg_slime",
    "egg_red",
    "egg_purple",
    "egg_spiders",
    "egg_worm",
    "evil_eye", --todo: fix evil eye's eyeball
    "gourd",
    "jar_of_urine",
    "moon",
    "potion_random_material",
    "potion_random_material",
    "potion_random_material",
    "potion_random_material",
    "potion_random_material",
    "potion_random_material",
    "potion_random_material",
    "potion_random_material",
    "potion_random_material",
    "potion_random_material",
    "potion_aggressive",
    "potion_aggressive",
    "potion_aggressive",
    "potion_aggressive",
    "potion_aggressive",
    "powder_stash",
    "powder_stash",
    "powder_stash",
    "powder_stash",
    "potion_secret",
    "safe_haven",
    "stonestone",
    "thunderstone",
    "thunderstone",
    "waterstone",
    "wandstone"
}

function set_board_message(message, routine)
    if message == last_board_message and routine == last_board_routine then return end

    local led_board = EntityGetWithName("led_board")
    if led_board ~= nil then
        ComponentSetValue2(
            EntityGetFirstComponentIncludingDisabled(led_board, "VariableStorageComponent", "message"), 
                "value_string",
                tostring(message))
        ComponentSetValue2(
            EntityGetFirstComponentIncludingDisabled(led_board, "VariableStorageComponent", "routine"), 
                "value_int",
                routine)
        ComponentSetValue2(
            EntityGetFirstComponentIncludingDisabled(led_board, "VariableStorageComponent", "reset"), 
                "value_bool",
                true)
    end

    last_board_message = message
    last_board_routine = routine
end

--set the clock display by passing it a string of up to four numerals
function set_board_clock(time_string)
    if time_string == last_clock_display then return end

    local clock = EntityGetWithName("clock")
    if clock ~= nil then
        local input = EntityGetFirstComponentIncludingDisabled(clock, "VariableStorageComponent", "input")
        if input ~= nil then
            ComponentSetValue2(input, "value_string", tostring(time_string))
        end
    end
end

function clock_sound_effect(sound_name)
    local clock = EntityGetWithName("clock")
    if clock ~= nil then
        GameEntityPlaySound(clock, sound_name)
    end
end

function randomize_start_position(player_id)
    local player_x, player_y = EntityGetTransform(player_id)
    if player_x == 0 then player_x = 1 end
    if player_y == 0 then player_y = 1 end
    local start_min_x = -95
    local start_max_x = 95
    local year, month, day, hour = GameGetDateAndTimeLocal()
    local elapsed = GameGetRealWorldTimeSinceStarted()
    if elapsed == 0 then elapsed = 1 end
    SetRandomSeed(1312 * elapsed * hour * player_x, 666 * elapsed * hour * player_y)
    local start_x = Random(start_min_x, start_max_x)
    local start_y = -212
    EntitySetTransform(player_id, start_x, start_y)

    EntityAddChild(
        player_id,
        EntityLoad("mods/noita-firefight/files/entities/teleport_gfx/teleport_gfx.xml", start_x, start_y))
end

function reset_firefight()
    fancy_pixels(
        "mods/noita-firefight/data/biome_impl/pixel_scenes/",
        "firefight_full_material_01.png",
        -512, -512, 
        "firefight_full_visual_01.png",
        "firefight_full_background_01.png",
        nil,
        nil,
        0)

    fancy_pixels(
        "mods/noita-firefight/data/biome_impl/pixel_scenes/",
        "firefight_full_material_02.png",
        0, -512,
        "firefight_full_visual_02.png",
        "firefight_full_background_02.png",
        nil,
        nil,
        0)
end

function clean_firefight_enemies()
    local enemies = EntityGetWithTag("enemy")
    for _, enemy in ipairs(enemies) do
        --if both enemy and player then it's a polymorphed player, don't kill them
        local game_stats_comp = EntityGetFirstComponentIncludingDisabled(enemy, "GameStatsComponent")
        if game_stats_comp ~= nil then
            local name = ComponentGetValue2(game_stats_comp, "name")
            if name == "player" then
                goto next_enemy
            end
        end
        EntityKill(enemy)
        ::next_enemy::
    end
end

function clean_firefight_entities()
    clean_firefight_enemies()

    local player_root = EntityGetRootEntity(EntityGetWithTag("player_unit")[1])
    local wands = EntityGetWithTag("wand")
    for _, wand in ipairs(wands) do
        local root = EntityGetRootEntity(wand)
        if root ~= player_root then
            EntityKill(wand)
        end
    end

    local spells = EntityGetWithTag("spell")
    for _, spell in ipairs(spells) do
        local spell_root = EntityGetRootEntity(spell)
        if spell_root ~= player_root then EntityKill(spell) end
        --local item_component = EntityGetFirstComponentIncludingDisabled(spell, "ItemComponent")
        --if item_component ~= nil then
        --    local has_been_picked_by_player = ComponentGetValue2(item_component, "has_been_picked_by_player")
        --    if not has_been_picked_by_player then
        --        EntityKill(spell)
        --    end
        --end
    end

    local projectiles = EntityGetWithTag("projectile")
    for _, projectile in ipairs(projectiles) do
        EntityKill(projectile)
    end

    local statues = EntityGetWithTag("statue_right")
    for _, statue in ipairs(statues) do
        EntityKill(statue)
    end
    GlobalsSetValue("PERK_THIS_ROUND", "0")
    load_right_statue()
end

function load_firefight_entities()
    --statue alters
    EntityLoad("mods/noita-firefight/files/entities/statue_left/statue_left.xml", -424, -321)
    EntityLoad("mods/noita-firefight/files/entities/statue_left/statue_left_player_label.xml", -424, -303)
    EntityLoad("mods/noita-firefight/files/entities/statue_left/statue_left_purchase.xml", -427, -321)
    EntityLoad("mods/noita-firefight/files/entities/statue_left/arrow_left.xml", -451, -326)
    EntityLoad("mods/noita-firefight/files/entities/statue_left/arrow_right.xml", -396, -326)
    load_right_statue()

    --scoreboard
    EntityLoad("mods/noita-firefight/files/entities/clock/clock.xml", -33, -318)
    EntityLoad("mods/noita-firefight/files/entities/led_board/board.xml", -51, -288)

    --force field
    EntityLoad("mods/noita-firefight/files/entities/force_field/start_area_force_field.xml", 0, -266)
end

function load_right_statue()
    local statue_right = EntityLoad("mods/noita-firefight/files/entities/statue_right/statue_right.xml", 424, -321)
    local label_sprite = EntityGetFirstComponentIncludingDisabled(statue_right, "SpriteComponent")
    if label_sprite ~= nil then
        EntitySetComponentIsEnabled(statue_right, label_sprite, true)
    end
end

function load_item_shop()
    dofile("mods/noita-firefight/files/scripts/store.lua")

    local dummy_shop_item = EntityGetWithTag("dummy_shop_item")[1]
    EntityKill(dummy_shop_item)

    fancy_pixels(
        "mods/noita-firefight/data/biome_impl/pixel_scenes/",
        "item_shop_material.png",
        -22, -464,
        "item_shop_visual.png",
        "",
        nil,
        nil,
        0)

    --[[local player = EntityGetWithTag("player_unit")[1]
    if player ~= nil then
        local x, y = EntityGetTransform(player)
        local time = GameGetRealWorldTimeSinceStarted()
        local frame = GameGetDateAndTimeUTC()
        SetRandomSeed(tonumber(y + time), tonumber(x + frame))
        local item_name = Items[Random(1, #Items)]
        local item = EntityLoad(tostring("data/entities/items/pickup/" .. item_name .. ".xml"), 0, -465)
        local item_cost = 100 + ((FIREFIGHT.round - 1) * 10)
        EntityAddComponent2(item, "ItemCostComponent", {
            cost = item_cost
        })
        EntityAddComponent2(item, "SpriteComponent", {
            _tags = "shop_cost",
            image_file = "data/fonts/font_pixel_white.xml",
            is_text_sprite = true,
            offset_x = 6,
            offset_y = 12,
            update_transform = true,
            update_transform_rotation = false,
            text = tostring(item_cost),
            z_index= -1
        })
        EntityAddComponent2(item, "LuaComponent", {
            script_item_picked_up="mods/noita-firefight/files/scripts/shop_item_pickup.lua"
        })
    end]]
end

function is_valid_entity(entity_id)
    return entity_id ~= nil and entity_id ~= 0
end

function seconds_to_clock_string(total_seconds)
    local total_seconds = total_seconds % 6000 --6000s is 99:59, max time display, so overflow
    local minutes = math.floor(total_seconds / 60)
    if minutes == 0 then minutes = "" end
    local seconds = total_seconds % 60
    if seconds < 10 then seconds = "0" .. tostring(seconds) end
    local time_string = tostring(minutes) .. tostring(seconds)
    return time_string
end

function start_area_force_field(status)
    local start_area_force_field = EntityGetWithName("start_area_force_field")
    if start_area_force_field ~= nil then
        local variable_components = EntityGetComponent(start_area_force_field, "VariableStorageComponent")
        for _, comp in ipairs(variable_components) do
            if ComponentGetValue2(comp, "name") == "mode" then
                ComponentSetValue2(comp, "value_string", status)
            end
        end
    end
end

function add_area_constraint(entity_id, constraint_name, min_x, max_x, min_y, max_y)
    local entity_x, entity_y = EntityGetTransform(entity_id)
    local start_constraint = EntityLoad(
        "mods/noita-firefight/files/entities/area_constraint/area_constraint.xml",
        entity_x, entity_y)
    EntitySetName(start_constraint, constraint_name)

    local variable_components = EntityGetComponent(start_constraint, "VariableStorageComponent")
    for _, comp in ipairs(variable_components) do
        local name = ComponentGetValue2(comp, "name")
        if name == "min_x" then
            ComponentSetValue2(comp, "value_int", min_x)
        elseif name == "max_x" then
            ComponentSetValue2(comp, "value_int", max_x)
        elseif name == "min_y" then
            ComponentSetValue2(comp, "value_int", min_y)
        elseif name == "max_y" then
            ComponentSetValue2(comp, "value_int", max_y)
        end
    end
    EntityAddChild(entity_id, start_constraint)
end

function remove_area_constraint(entity_id, constraint_name)
    local children = EntityGetAllChildren(entity_id)
    for _, child in ipairs(children) do
        if EntityGetName(child) == constraint_name then
            EntityKill(child)
        end
    end
end

function roll_random_loot(x, y)
    y = y - 10
    SetRandomSeed(x, y)
    local random = Random(1, 100)

    --wand drop 8%
    if random <= 8 then
        local wand = EntityLoad("mods/noita-firefight/files/entities/base_wand/base_wand.xml", x, y)
        EntityAddTag(wand, "wand")
        toss_item(wand)
    --spell drop 20%
    elseif random <= 28 then
        dofile("mods/noita-firefight/files/scripts/store.lua")
        dofile("data/scripts/gun/gun_actions.lua")
        SetRandomSeed(x + GameGetRealWorldTimeSinceStarted(), y + GameGetFrameNum())
        local level = math.min(5, 1 + math.floor(FIREFIGHT.round / 3))
        local combined_spells = {}
        for i = 1, level do
            for _, spell in pairs(SpellTiers[i]) do
                table.insert(combined_spells, spell)
            end
        end
        local combined_spells_index = Random(1, #combined_spells)
        local action_index = combined_spells[combined_spells_index]
        local action = actions[action_index]
        action_name = string.lower(action.id)
        local spell = CreateItemActionEntity(action_name, x, y)
        EntityAddTag(spell, "spell")
        toss_item(spell)
    end
end

function random_perk()
    local perk = Perks[Random(1, #Perks)]
    return perk
end

function grant_tinker(player_id)
    local tinker_entity = EntityCreateNew("tinker_game_effect")
    EntityAddComponent2(tinker_entity, "GameEffectComponent", {
        frames = -1,
        effect = "EDIT_WANDS_EVERYWHERE"
    })
    EntityAddChild(player_id, tinker_entity)
end

function revoke_tinker(player_id)
    local children = EntityGetAllChildren(player_id)
    for _, child in ipairs(children) do
        if EntityGetName(child) == "tinker_game_effect" then
            EntityKill(child)
        end
    end
end

function spawn_enemy(x, y, level, options)
    if options == nil then options = {} end

    --determine enemy tier
    local tier = Random(1, level)

    --pick an enemy from the selected tier
    local enemy_name = EnemyTiers[tier][Random(1, #EnemyTiers[tier])]

    --portal
    local portal = EntityLoad("mods/noita-firefight/files/entities/spawn_portal/spawn_portal.xml", x, y)
    --tell portal what entity file to load
    local var_comps = EntityGetComponent(portal, "VariableStorageComponent")
    for _, var_comp in ipairs(var_comps) do
        if ComponentGetValue2(var_comp, "name") == "file_path" then
            ComponentSetValue2(var_comp, "value_string", tostring("data/entities/animals/" .. enemy_name .. ".xml"))
        end
    end

    for option_name, option_setting in pairs(options) do
        if option_name == "from_user" then
            EntityAddComponent2(portal, "VariableStorageComponent", {
                name = "from_name",
                value_string = tostring(option_setting)
            })
        end
    end
end

function restore_health_and_abilities(player_id)
    --refresher entity handles time-offset drops of heart and spell refresh
    local refresher = EntityLoad("mods/noita-firefight/files/entities/refresher/refresher.xml")
    EntityAddChild(player_id, refresher)
end

function get_firefight()
    dofile("mods/noita-firefight/files/scripts/store.lua")
    return FIREFIGHT
end

function update_local_player_stats(updates)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    for k, v in pairs(updates) do
        FIREFIGHT[k] = v
    end
end

function update_player_stats(user_id, updates)
    local player_list = get_player_list()
    for _, player in pairs(player_list) do
        if player.userId == user_id then
            for k, v in pairs(updates) do
                player[k] = v
            end
        end
    end
    set_player_list(player_list)
end

function get_player_list(include_dead)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    if FIREFIGHT == nil then return nil end

    local player_list = json.decode(FIREFIGHT.PlayerList)
    if include_dead ~= nil and include_dead == false then
        local culled_player_list = {}
        for _, player in pairs(player_list) do
            if player.alive == true then
                table.insert(culled_player_list, player)
            end
        end
        return culled_player_list
    else
        return player_list
    end

    --return json.decode(FIREFIGHT.PlayerList)
end

function set_player_list(new_list)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    FIREFIGHT.PlayerList = json.encode(new_list)
end

function get_player_by_user_id(user_id)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    if FIREFIGHT == nil then return nil end
    local player_list = json.decode(FIREFIGHT.PlayerList)
    for _, player in pairs(player_list) do
        if player.userId == user_id then return player end
    end
    return nil
end

function get_player_by_name(player_name)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    if FIREFIGHT == nil then return nil end
    local player_list = json.decode(FIREFIGHT.PlayerList)
    for _, player in pairs(player_list) do
        if player.name == player_name then return player end
    end
    return nil
end

--get current send cost for player
function get_send_cost_for_player(user_id)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    local player_list = json.decode(FIREFIGHT.PlayerList)
    for _, player in pairs(player_list) do
        if player.userId == user_id then
            if player.round_sends == nil then player.round_sends = 0 end
            return round_base_send_cost() + (2 * FIREFIGHT.round * player.round_sends)
        end
    end
    return nil
end

--local client player isn't in the player list, but this can be used to fake it
function get_local_player()
    dofile("mods/noita-firefight/files/scripts/store.lua")
    local local_player = {
        player_name = FIREFIGHT.player_name,
        score = FIREFIGHT.score,
        money = FIREFIGHT.money,
        userId = FIREFIGHT.userId,
        round_sends = FIREFIGHT.round_sends
    }
    return local_player
end

--increment send cost for player and then get cost
function get_new_send_cost_for_player(user_id)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    local player_list = json.decode(FIREFIGHT.PlayerList)
    if player_list[tonumber(user_id)].round_sends == nil then
        player_list[tonumber(user_id)].round_sends = 0
    else
        player_list[tonumber(user_id)].round_sends = player_list[tonumber(user_id)].round_sends + 1
    end
    FIREFIGHT.PlayerList = json.encode(player_list)
    return round_base_send_cost() + (2 * FIREFIGHT.round * player_list[tonumber(user_id)].round_sends)
end

--cost of the first enemy send of the current round
function round_base_send_cost()
    dofile("mods/noita-firefight/files/scripts/store.lua")
    return 10 * math.max(1, FIREFIGHT.round)
end

function queue_custom_mod_event(event_name, data)
    --print("queue_custom_mod_event")
    dofile("mods/noita-together/files/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    local payload = { name = event_name }
    for k, v in pairs(data) do
        payload[k] = v
    end
    local queue = json.decode(NT.wsQueue)
    table.insert(queue, { 
        event = "CustomModEvent",
        payload = payload
    })
    NT.wsQueue = json.encode(queue)
end

--a probably-totally-unique string, maybe
function generate_request_id()
    local y, m, d, hour, m, seconds = GameGetDateAndTimeLocal()
    local player = EntityGetWithTag("player_unit")[1]
    if player == nil then return end
    local player_x, player_y = EntityGetTransform(player)
    return tostring(hour * seconds) .. tostring(player_x) .. tostring(player_y)
end

function send_echo_request()
    dofile("mods/noita-firefight/files/scripts/store.lua")
    local echo_request_id = generate_request_id()
    FIREFIGHT.echo_request_id = echo_request_id
    queue_custom_mod_event("FirefightEcho", { requestId = echo_request_id })
end

function send_echo_response(data)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    for _, player in pairs(json.decode(FIREFIGHT.PlayerList)) do
        if player.userId == data.userId then
            local payload = player
            payload["requestId"] = data.requestId
            payload["round"] = FIREFIGHT.round
            payload["phase"] = FIREFIGHT.phase
            print("phase: " .. tostring(FIREFIGHT.phase))
            if FIREFIGHT.winner_name ~= nil then
                payload["winner_name"] = FIREFIGHT.winner_name
            end
            payload["time_since_last_phase_start"] = GameGetRealWorldTimeSinceStarted() - FIREFIGHT.last_phase_start
            queue_custom_mod_event("FirefightEchoResponse", payload)
        end
    end
end

function spawn_sent_enemy(from_player_name)
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")

    local player = EntityGetWithTag("player_unit")[1]
    if player == nil then return end
    local player_x, player_y = EntityGetTransform(player)
    local spawn_x, spawn_y = random_point_near(player_x, player_y, 10, 30)

    local level = math.max(1, math.floor(FIREFIGHT.round / 3))
    local level_boost_roll = Random(100)
    if level_boost_roll < 2 then
        level = level + 2
    elseif level_boost_roll < 12 then
        level = level + 1
    end

    spawn_enemy(spawn_x, spawn_y, level, { from_user = from_player_name })
end

function random_point_near(x, y, min, max)
    local spawn_offset_x, spawn_offset_y = Random(min, max), Random(min, max)
    if Random() > 0.5 then spawn_offset_x = -spawn_offset_x end
    if Random() > 0.5 then spawn_offset_y = -spawn_offset_y end
    local spawn_x = x + spawn_offset_x
    local spawn_y = y + spawn_offset_y
    return spawn_x, spawn_y
end

function initialize_left_statue()
    local statue_left = nil
    local statue_left_entities = EntityGetWithTag("statue_left")
    for _, entity in ipairs(statue_left_entities) do
        if EntityGetName(entity) == "statue_left" then
            statue_left = entity
        end
    end
    if statue_left == nil then return end

    EntityAddComponent2(statue_left, "LuaComponent", {
        execute_every_n_frame = 1,
        remove_after_executed = true,
        script_source_file="mods/noita-firefight/files/entities/statue_left/statue_left_init.lua"
    })
end

function copy_component_to_entity(old_comp, entity)
    local members = ComponentGetMembers(old_comp)
    if members == nil then return end
    local new_comp = EntityAddComponent(entity, ComponentGetTypeName(old_comp))
    for k, v in pairs(members) do
        --avoid values that don't translate successfully with component calls
        if k ~= "m_collision_angles"
        and k ~= "m_cached_image_animation"
        and k ~= "mSprite" then
            ComponentSetValue2(new_comp, k, ComponentGetValue2(old_comp, k))
        end
    end
    return new_comp
end

--pause removal and aquisition of status effects by disabling the component
function toggle_status_effect_component(player_id, on)
    if on == false then
        --print("adding protection")
        local protection_entity = EntityLoad("data/entities/misc/effect_protection_all.xml", 0, 0)
        EntityAddChild(player_id, protection_entity)
        local protection_poly_entity = EntityLoad("data/entities/misc/effect_protection_polymorph.xml", 0, 0)
        EntityAddChild(player_id, protection_poly_entity)
    else
        --print("removing protection")
        local children = EntityGetAllChildren(player_id)
        for _, child in ipairs(children) do
            local game_effect_comp = EntityGetFirstComponentIncludingDisabled(child, "GameEffectComponent")
            if game_effect_comp ~= nil then
                local effect = ComponentGetValue2(game_effect_comp, "effect")
                if effect ~= nil then
                    if effect  == "PROTECTION_ALL"
                    or effect == "PROTECTION_POLYMORPH" then
                        EntityKill(child)
                    end
                end
            end
        end
    end
    local status_effect_comp = EntityGetFirstComponentIncludingDisabled(player_id, "StatusEffectDataComponent")
    if status_effect_comp == nil then return end
    EntitySetComponentIsEnabled(player_id, status_effect_comp, on)
end

--clear all status effects by replacing the component with a new one
function reset_status_effect_component(player_id)
    local status_effect_comp = EntityGetFirstComponentIncludingDisabled(player_id, "StatusEffectDataComponent")
    if status_effect_comp ~= nil then
        EntityRemoveComponent(player_id, status_effect_comp)
    end
    EntityAddComponent2(player_id, "StatusEffectDataComponent")
end

function wash_stains(entity_id)
    EntityAddRandomStains(entity_id, CellFactory_GetType("wash"), 400)
end

function set_iframes(entity, frames)
    local damage_comp = EntityGetFirstComponent(entity, "DamageModelComponent")
    if damage_comp ~= nil then
        ComponentSetValue2(damage_comp, "invincibility_frames", frames)
    end
end

function announce_self()
    dofile("mods/noita-together/files/scripts/utils.lua")
    local serialized = SerializeWands()
    queue_custom_mod_event("PlayerInven", { inven = serialized })
end

function toss_item(item)
    EntityAddTag(item, "enabled_in_world")

    local item_component = EntityGetFirstComponent(item, "ItemComponent")
    local simple_physics_comp = EntityGetFirstComponent(item, "SimplePhysicsComponent")
    local velocity_comp = EntityGetFirstComponent(item, "VelocityComponent")

    if item_component ~= nil then
        ComponentSetValue2(item_component, "play_hover_animation", false)
    end

    if simple_physics_comp == nil then
        EntityAddComponent2(item, "SimplePhysicsComponent", {})
    end

    if velocity_comp == nil then
        velocity_comp = EntityAddComponent2(item, "VelocityComponent", {})
    end

    local x_velocity = Random(0, 100) - 50
    ComponentSetValue2(velocity_comp, "mVelocity", x_velocity, -150)
    ComponentSetValue2(velocity_comp, "mPrevVelocity", x_velocity, -150)
end

function send_countdown_ready()
    queue_custom_mod_event("FirefightReadyForCountdown", {})
end

function everyone_is_ready_for_countdown()
    dofile("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    for _, player in pairs(json.decode(FIREFIGHT.PlayerList)) do
        if player.alive == true and player.ready == false then
            return false
        end
    end
    --send a ready-up just in case this client was the last one ready
    send_countdown_ready()
    return true
end

function undo_polymorphs()
    if last_unpoly_frame > frame_num - 2 then return end
    local sheepies = EntityGetWithTag("polymorphed")
    for _, sheepie in ipairs(sheepies) do
        local children = EntityGetAllChildren(sheepie)
        if children ~= nil then
            for _, child in ipairs(children) do
                local game_effect_comp = EntityGetFirstComponentIncludingDisabled(child, "GameEffectComponent")
                if game_effect_comp ~= nil then
                    local effect = ComponentGetValue2(game_effect_comp, "effect")
                    if effect == "POLYMORPH"
                    or effect == "POLYMORPH_RANDOM"
                    or effect == "POLYMORPH_UNSTABLE" then
                        last_unpoly_frame = frame_num
                        ComponentSetValue2(game_effect_comp, "frames", 1)
                    end
                end
            end
        end
    end
end

function play_fight_music()
    local easy_tracks = {
        "music/rainforest/_01",
        "music/snowcastle/_06",
        "music/rainforest/_01",
        "music/wandcave/02",
        "music/excavationsite/11"
    }

    local hard_tracks = {
        "music/vault/05",
        "music/vault/04",
        "music/vault/03",
        "music/vault/01",
        "music/rainforest/03",
        "music/side_biome_action/00",
        "music/tower/00"
    }

    --other tracks of note:
    --music/crypt/04 <-- slow and doomy
    --music/crypt/_03 <-- oh lawd he comin

    local track = easy_tracks[Random(1, #easy_tracks)]
    if FIREFIGHT.round > 5 then
        track = hard_tracks[Random(1, #hard_tracks)]
    end

    GameTriggerMusicFadeOutAndDequeueAll(1)
    GameTriggerMusicEvent(track, false, player_x, player_y)
end

function play_tinker_music()
    local track = "noita_firefight/tinker_01"
    if FIREFIGHT.round % 2 == 0 then track = "noita_firefight/tinker_02" end

    GameTriggerMusicFadeOutAndDequeueAll(1)
    GamePlaySound(
        "mods/noita-firefight/files/audio/noita_firefight.snd",
        track,
        0, -212)
end