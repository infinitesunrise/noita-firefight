function StartRun()
    dofile_once("mods/noita-firefight/files/scripts/store.lua")

    local player = GetPlayer()
    if (player ~= nil and player > 0) then
        if (NT ~= nil and NT.run_started == false) then
            GamePrintImportant("Game Started", "Have fun!")
            NT.run_started = true
        end
        
        _start_run = false
        _started = true

        if FIREFIGHT ~= nil and _started == true and FIREFIGHT.phase == "pregame" then
            FIREFIGHT.phase = "countdown"
            FIREFIGHT.game_start_time = GameGetRealWorldTimeSinceStarted()
            FIREFIGHT.last_phase_start = FIREFIGHT.game_start_time
            FIREFIGHT.round = 1

            play_fight_music()
        end
    end
end

function FinishRun(winner_name)
    if (NT.run_ended or not NT.run_started) then return end
    dofile_once("mods/noita-firefight/files/scripts/store.lua")
    dofile_once("mods/noita-firefight/files/scripts/utils.lua")
    FIREFIGHT.phase = "post"
    FIREFIGHT.winner_name = winner_name
    --NT.run_ended = true
    clean_firefight_enemies()
    set_board_message(winner_name .. " wins! ", LEDRoutine.SCROLL)
    set_board_clock("0000")

    --SendWsEvent({event="PlayerDeath", payload={isWin=false, gameTime=GameGetFrameNum()}})
    --SendWsEvent({event="RunOver", payload={}})
end

function IsPlayerDead()
    local player = GetPlayer()
    local damage_models = nil
    local platforming_comp = nil
    if (player ~= nil) then
        damage_models = EntityGetFirstComponent(player, "DamageModelComponent")
        platforming_comp = EntityGetFirstComponent(player, "CharacterPlatformingComponent")
    end

    if (damage_models ~= nil and platforming_comp ~= nil) then
        local curHp = ComponentGetValue2(damage_models, "hp")
        if (curHp < 0.0) then
            ComponentSetValue2(damage_models, "wait_for_kill_flag_on_death", true)
            ComponentSetValue2(damage_models, "hp", 0.0)
            EntitySetComponentIsEnabled(player, platforming_comp, false)

            dofile_once("mods/noita-firefight/files/scripts/store.lua")
            FIREFIGHT.alive = false
            LastUpdate.curHp = 0

            local cx, cy = GameGetCameraPos()
            GameSetCameraFree(true)

            RecursiveSpriteDisable(player)
            --RecursiveSpriteRemove(player)

            local children = EntityGetAllChildren(player)
            for _, child in ipairs(children) do
                if EntityGetName(child) == "cape" then
                    EntityKill(child)
                end
            end

            msg = "You died"
            GamePrintImportant(msg, "")

            queue_custom_mod_event("FirefightPlayerDeath", { userId = FIREFIGHT.userId })

            local aliveCount = 0
            local winnerName = ""
            for k, v in pairs(PlayerList) do
                if (v.curHp > 0) then
                    aliveCount = aliveCount + 1
                    winnerName = v.name
                end
            end
            --someone else just won
            if (aliveCount == 1) then
                msg = winnerName .. " has won"
                GamePrintImportant(msg, "")
                NT.end_msg = msg
                FinishRun(winnerName)
            end

            --EntityLoad("mods/noita-together/files/entities/death_cam.xml", cx, cy)
        end
    end
end

--[[function get_alive_players()
    alive_players = {}
    for k, v in pairs(PlayerList) do
        if (v.curHp > 0) then
            table.insert(alive_players, v)
        end
    end
    return alive_players
end]]

function SpawnPlayerGhosts(player_list)
    dofile_once("mods/noita-firefight/files/scripts/store.lua")
    dofile("mods/noita-together/files/scripts/json.lua")
    local arena_players = json.decode(FIREFIGHT.PlayerList)
    for _, player in ipairs(arena_players) do
        player_list[player.userId].alive = player.alive
    end
    for userId, player in pairs(player_list) do
        print(player.name .. ": " .. tostring(player.alive))
        if player.alive == true then
            SpawnPlayerGhost(player, userId)
        end
    end
end

function RecursiveSpriteDisable(entity)
    local sprite_components = EntityGetAllComponents(entity)
    if sprite_components ~= nil then
        for _, sprite in ipairs(sprite_components) do
            EntitySetComponentIsEnabled(entity, sprite, false)
        end
    end
    local children = EntityGetAllChildren(entity)
    if children ~= nil then
        for _, child in ipairs(children) do
            RecursiveSpriteDisable(child)
        end
    end
end

function RecursiveSpriteRemove(entity)
    local sprite_components = EntityGetAllComponents(entity)
    if sprite_components ~= nil then
        for _, sprite in ipairs(sprite_components) do
            EntityRemoveComponent(entity, sprite)
        end
    end
    local children = EntityGetAllChildren(entity)
    if children ~= nil then
        for _, child in ipairs(children) do
            RecursiveSpriteRemove(child)
        end
    end
end

--use new counter index for wand array instead of inventory_quick's index
function GetPlayerWands()
    local player = GetPlayer()
    if (player == nil) then return {} end
    local childs = EntityGetAllChildren(player)
    local inven = nil
    if childs ~= nil then
        for _, child in ipairs(childs) do
            if EntityGetName(child) == "inventory_quick" then
                inven = child
            end
        end
    end
    local wands = {}
    if inven ~= nil then
        local items = EntityGetAllChildren(inven) or {}
        local index = 1
        for _, child_item in ipairs(items) do
            if EntityHasTag(child_item, "wand") then
                wands[index] = child_item
                index = index + 1
            end
        end
    end

    return wands or nil
end

--add grip_x and grip_y to returned wand stats to inform wand sprite offset
function GetWandStats(id)
    local serialized = {}

    local ability_comp = EntityGetFirstComponentIncludingDisabled(id, "AbilityComponent")
    serialized.sprite = ComponentGetValue2(ability_comp, "sprite_file")
    serialized.uiName = ComponentGetValue2(ability_comp, "ui_name")
    serialized.manaMax = ComponentGetValue2(ability_comp, "mana_max")
    serialized.manaChargeSpeed = ComponentGetValue2(ability_comp, "mana_charge_speed")

    local sprite_comp = EntityGetFirstComponentIncludingDisabled(id, "SpriteComponent")
    serialized.offset_x = tonumber(ComponentGetValue2(sprite_comp, "offset_x"))
    serialized.offset_y = tonumber(ComponentGetValue2(sprite_comp, "offset_y"))

    serialized.reloadTime = ComponentObjectGetValue2(ability_comp, "gun_config", "reload_time")
    serialized.actionsPerRound = ComponentObjectGetValue2(ability_comp, "gun_config", "actions_per_round")
    serialized.deckCapacity = ComponentObjectGetValue2(ability_comp, "gun_config", "deck_capacity")
    serialized.shuffleDeckWhenEmpty = ComponentObjectGetValue2(ability_comp, "gun_config", "shuffle_deck_when_empty")

    serialized.spreadDegrees = ComponentObjectGetValue2(ability_comp, "gunaction_config", "spread_degrees")
    serialized.speedMultiplier = ComponentObjectGetValue2(ability_comp, "gunaction_config", "speed_multiplier")
    serialized.fireRateWait = ComponentObjectGetValue2(ability_comp, "gunaction_config", "fire_rate_wait")
    local item_comp = EntityGetFirstComponentIncludingDisabled(id, "ItemComponent")
    serialized.inven_slot = ComponentGetValue2(item_comp, "inventory_slot")
    return serialized
end