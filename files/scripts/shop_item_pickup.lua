--[[GamePrint("RUNNING PICKUP SCRIPT")
dofile_once("mods/noita-firefight/files/scripts/utils.lua")

function item_pickup(entity_item, entity_who_picked, item_name)
    local item_cost_comp = EntityGetFirstComponentIncludingDisabled(entity_item, "ItemCostComponent")
    if item_cost_comp == nil then return end
    local cost = ComponentGetValue2(item_cost_comp, "cost")
    if cost == nil then return end
    local cost_sprite = EntityGetFirstComponentIncludingDisabled(entity_item, "SpriteComponent", "shop_cost")
    if cost_sprite == nil then return end
    local wallet_comp = EntityGetFirstComponentIncludingDisabled(entity_who_picked, "WalletComponent")
    if wallet_comp == nil then return end
    local money = ComponentGetValue2(wallet_comp, "money")
    local new_money = tonumber(money - cost)

    queue_custom_mod_event("FirefightStatUpdate", { money = new_money })

    EntityRemoveComponent(entity_item, item_cost_comp)
    EntityRemoveComponent(entity_item, cost_sprite)

    local physics_body_comp = EntityGetFirstComponentIncludingDisabled(entity_item, "PhysicsBodyComponent")
    if physics_body_comp ~= nil then
        ComponentSetValue2(physics_body_comp, "is_static", false)
    end
end]]