--all credit to bruham for the below code
--allows for repeated reloading of pixel scenes

function get_storage( hooman, name )
	local comps = EntityGetComponentIncludingDisabled( hooman, "VariableStorageComponent" ) or {}
	if( #comps > 0 ) then
		for i,comp in ipairs( comps ) do
			if( ComponentGetValue2( comp, "name" ) == name ) then
				return comp
			end
		end
	end
	
	return nil
end

function set_lps_memo( data )
	if( data == nil ) then
		return nil
	end
	
	local data_raw = "|"
	local lcl_data = data
	for i,subdata in pairs( lcl_data ) do
		data_raw = data_raw..":"..tostring( i )
		for e,values in pairs( subdata ) do
			data_raw = data_raw..":&"..tostring( e )
			for k,value in pairs( values ) do
				data_raw = data_raw.."&"..k.."&"..value
			end
			data_raw = data_raw.."&"
		end
		
		data_raw = data_raw..":|"
	end
	
	return data_raw
end

function fancy_pixels( main_path, mtr_file, pos_x, pos_y, vis_file, back_file, c2m_tbl, pos_z, no_edge )
	local function c2m_packer( data )
		if( data == nil ) then
			return nil
		end
		
		local data_raw = "!"
		
		for i,value in pairs( data ) do
			data_raw = data_raw..i.."!"..value.."!"
		end
		
		return data_raw
	end
	
	if( main_path == nil or mtr_file == nil or pos_x == nil or pos_y == nil ) then
		return
	end
	
	local world_entity = GameGetWorldStateEntity() or 0
	if( world_entity == 0 ) then
		return
	end
	
	local storage_list = get_storage( world_entity, "loadpixels_list" )
	if( storage_list == nil ) then
		return
	end
	
	vis_file = vis_file or "_"
	back_file = back_file or "_"
	c2m_tbl = c2m_packer( c2m_tbl ) or "_"
	pos_z = pos_z or "_"
	no_edge = no_edge or "_"
	
	local list = ":"..main_path..":"..mtr_file..":"..pos_x..":"..pos_y..":"..vis_file..":"..back_file..":"..c2m_tbl..":"..pos_z..":"..no_edge..":"
	ComponentSetValue2( storage_list, "value_string", ComponentGetValue2( storage_list, "value_string" )..list.."|" )
end

function OnWorldPostUpdate()
	local function XD_extractor( data_raw, use_string, string_coded )
		if( data_raw == nil ) then
			return nil
		end
		use_string = use_string or false
		string_coded = string_coded or false
		
		local data = {}
		
		for subdata_raw in string.gmatch( data_raw, "([^|]+)" ) do
			local subdata = {}
			local done = false
			for value in string.gmatch( subdata_raw, "([^:]+)" ) do
				if( not( string_coded ) or done ~= false ) then
					table.insert( subdata, use_string and value or tonumber( value ))
				else
					done = value
				end
			end
			if( string_coded ) then
				data[ done ] = #subdata > 1 and subdata or subdata[1]
			else
				table.insert( data, subdata )
			end
		end
		
		return data
	end
	
	local function get_lps_memo( data_raw )
		if( data_raw == nil ) then
			return nil
		end
		
		local data = {}
		
		for subdata_raw in string.gmatch( data_raw, "([^|]+)" ) do
			local subdata = {}
			local main_path = false
			for values in string.gmatch( subdata_raw, "([^:]+)" ) do
				if( main_path ~= false ) then
					local mtr_file = false
					local pos = ""
					local count = 0
					for value in string.gmatch( values, "([^&]+)" ) do
						if( mtr_file ~= false ) then
							if( count%2 == 0 ) then
								subdata[ mtr_file ][ pos ] = tonumber( value )
							else
								pos = value
							end
						else
							mtr_file = value
							subdata[ mtr_file ] = {}
						end
						
						count = count + 1
					end
				else
					main_path = values
				end
			end
			data[ main_path ] = subdata
		end
		
		return data
	end
	
	local world_entity = GameGetWorldStateEntity() or 0
	if( world_entity == 0 ) then
		return
	end
	
	gonna_save = gonna_save or false
	
	local default_table = "|:b:&a&l&1&:|"
	local storage_memo = get_storage( world_entity, "loadpixels_memo" )
	if( storage_memo == nil ) then
		storage_memo = EntityAddComponent( world_entity, "VariableStorageComponent", 
		{
			name = "loadpixels_memo",
			value_string = default_table,
		})
	end
	
	local default_value = "|"
	local storage_list = get_storage( world_entity, "loadpixels_list" )
	if( storage_list == nil ) then
		storage_list = EntityAddComponent( world_entity, "VariableStorageComponent", 
		{
			name = "loadpixels_list",
			value_string = default_value,
		})
	end
	local list = ComponentGetValue2( storage_list, "value_string" )
	if( list ~= default_value ) then
		list = XD_extractor( list, true )
		
		local function fletcher_does_magic( str, is_huge )
			is_huge = is_huge or false

			local a, b, c = 0, 0, ( is_huge and 65535 or 4294967295 )
			for i = 1,#str do
				a = ( a + string.byte( str, i ))%c
				b = ( a + b )%c
			end
			return ( is_huge and tostring( b*c + a ) or ( b*c + a ))
		end
		
		local function c2m_extractor( data_raw )
			if( data_raw == nil ) then
				return nil
			end
			
			local data = {}
			
			local temp = 0
			for value in string.gmatch( data_raw, "([^!]+)" ) do
				if( temp ~= 0 ) then
					data[ temp ] = tostring( value )
					temp = 0
				else
					temp = tostring( value )
				end
			end
			
			return data
		end
		
		local function list_checker( value )
			return ( value ~= "_" and value or nil )
		end
		
		for num,scene in ipairs( list ) do
			local main_path = scene[1]
			local mtr_file = scene[2]
			local pos_x = scene[3]
			local pos_y = scene[4]
			local vis_file = list_checker( scene[5] )
			local back_file = list_checker( scene[6] )
			local c2m_tbl = c2m_extractor( list_checker( scene[7] ))
			local pos_z = list_checker( scene[8] )
			local no_edge = list_checker( scene[8] ) ~= nil
			
			memo = memo or get_lps_memo( ComponentGetValue2( storage_memo, "value_string" ))
			gonna_save = true
			
			local check = {
				fletcher_does_magic( main_path, true ),
				tostring( fletcher_does_magic( mtr_file )),
				pos_x..pos_y,
			}
			if( memo[ check[1] ] == nil ) then
				memo[ check[1] ] = {}
			end
			if( memo[ check[1] ][ check[2] ] == nil ) then
				memo[ check[1] ][ check[2] ] = {}
			end
			if( memo[ check[1] ][ check[2] ][ check[3] ] == nil ) then
				memo[ check[1] ][ check[2] ][ check[3] ] = 1
			else
				local limit = 40
				local temp = { limit^2, limit^3, limit^4, }
				local count = memo[ check[1] ][ check[2] ][ check[3] ]
				
				local _, how_many = string.gsub( main_path, "/", "" )
				if( how_many > 3 ) then
					if( count < temp[3] ) then
						local actual_count = { count%limit, math.floor(( count%temp[1] )/limit ), math.floor(( count%temp[2] )/temp[1] ), math.floor(( count%temp[3] )/temp[2] ), }
						local thing = "/"
						for i,cnt in ipairs( actual_count ) do
							for e = 1,cnt do
								thing = thing.."/"
							end
							
							local k = 0
							main_path = string.gsub( main_path, "/", function( m )
								k = k + 1
								if( k == ( 5 - i )) then
									return thing
								end
							end )
							thing = "/"
						end
					else
						print( "[SNM ERROR] Repetition Limit Has Been Exceeded" )
					end
				else
					print( "[SNM ERROR] Main Path Is Not Complex Enough" )
					GamePrint( "[SNM ERROR] Main Path Is Not Complex Enough" )
				end
				
				memo[ check[1] ][ check[2] ][ check[3] ] = count + 1
			end
			LoadPixelScene( main_path..mtr_file, vis_file ~= nil and main_path..vis_file or "", pos_x, pos_y, back_file ~= nil and main_path..back_file or "", true, no_edge, c2m_tbl, pos_z )
		end
		
		ComponentSetValue2( storage_list, "value_string", default_value )
	end
	
	local no_saving = ModSettingGetNextValue( "snm.NO_SAVING" )
	if( instasave == nil ) then
		instasave = no_saving
	end
	
	if(( instasave ~= no_saving ) or ( gonna_save and not( no_saving ))) then
		ComponentSetValue2( storage_memo, "value_string", set_lps_memo( memo ) or default_table )
		gonna_save = false
		instasave = nil
	end
end

function OnPausedChanged( is_paused, is_inventory_pause )
	if( is_paused or is_inventory_pause ) then
		if( gonna_save ) then
			ComponentSetValue2( get_storage( GameGetWorldStateEntity(), "loadpixels_memo" ), "value_string", set_lps_memo( memo ) or "|:b:&a&l&1&:|" )
			gonna_save = false
		end
	end
end