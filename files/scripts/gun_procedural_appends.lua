function generate_empty_gun_and_throw(cost, level)
	local entity_id = GetUpdatedEntityID()
	local x, y = EntityGetTransform(entity_id)
	SetRandomSeed(x, y)
	local gun = get_gun_data(cost, level, false)
	make_wand_from_gun_data(gun, entity_id, level)
    GameShootProjectile(0, x, y, x, y + 50, entity_id)
end