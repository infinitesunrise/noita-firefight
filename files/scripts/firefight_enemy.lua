--adjusts enemy behaviors to firefight mode

local enemy = GetUpdatedEntityID()

--general enemy tag for cleanup at round end
EntityAddTag(enemy, "enemy")

--death script to do item drops and player stat updates
EntityLoadToEntity("mods/noita-firefight/files/entities/enemy_death/enemy_death.xml", enemy)

--all enemies are on the same team
local genome_component = EntityGetFirstComponentIncludingDisabled(enemy, "GenomeDataComponent")
if genome_component ~= nil then
    ComponentSetValue2(genome_component, "herd_id", 1)
end

--aggromaximus
local ai_component = EntityGetFirstComponentIncludingDisabled(enemy, "AnimalAIComponent")
if ai_component ~= nil then
    ComponentSetValue2(ai_component, "creature_detection_range_x", 2000)
    ComponentSetValue2(ai_component, "creature_detection_range_y", 2000)
    ComponentSetValue2(ai_component, "sense_creatures", true)
    ComponentSetValue2(ai_component, "sense_creatures_through_walls", true)
    ComponentSetValue2(ai_component, "aggressiveness_min", 100)
    ComponentSetValue2(ai_component, "aggressiveness_max", 100)
end

--check for options and act on any found
local var_comps = EntityGetComponentIncludingDisabled(entity_id, "VariableStorageComponent")
if var_comps ~= nil then
    for _, var_comp in ipairs(var_comps) do
        local name = ComponentGetValue2(var_comp, "name")
        --a sender name, displayed via some graphics
        if name == "from_name" then
            local sent_enemy_gfx = EntityLoad(
                "mods/noita-firefight/files/entities/sent_enemy_gfx/sent_enemy_gfx.xml",
                0, 0)
            EntityAddComponent2(sent_enemy_gfx, "VariableStorageComponent", {
                name = "from_name",
                value_string = ComponentGetValue2(var_comp, "value_string")
            })
            EntityAddChild(enemy, sent_enemy_gfx)
        end
    end
end