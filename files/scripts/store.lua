if FIREFIGHT then
    return
end
dofile("mods/noita-together/files/stringstore/stringstore.lua")
dofile("mods/noita-together/files/stringstore/noitaglobalstore.lua")
FIREFIGHT = stringstore.open_store(stringstore.noita.global("FIREFIGHT_STORE"))

if (FIREFIGHT.initialized ~= true) then
    FIREFIGHT.userId = nil                      --copy of NT userId
    FIREFIGHT.player_name = nil                 --twitch name
    FIREFIGHT.round = 0                         --game round
    FIREFIGHT.round_sends = 0                   --how many enemies have been sent at this client by other players in the current round
    FIREFIGHT.score = 0                         --total money earned (for now)
    FIREFIGHT.money = 0                         --current money
    FIREFIGHT.phase = "pregame"                 --round phase
    FIREFIGHT.game_start_time = 0               --local real world time of game start
    FIREFIGHT.last_phase_start = 0              --local real world time of last phase start
    FIREFIGHT.echo_request_id = nil             --id sent with info echo requests to identify responses
    FIREFIGHT.perk_this_round = false           --did the player pick up a perk this round
    FIREFIGHT.PlayerList = "{}"                 --similar to NT PlayerList, but firefight-specific 
    FIREFIGHT.alive = true                      --is player alive
    FIREFIGHT.winner_name = nil                 --if game is over, who won? for game clients who start a new game too early afer match end.
    FIREFIGHT.ready = false                     --is client done with tinker phase and awaiting countdown phase
    FIREFIGHT.initialized = true                --is this object initialized
    GlobalsSetValue("noita-firefight.player_spawned", "1")
end