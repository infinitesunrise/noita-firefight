_started = true

function OnPlayerSpawned(player_entity)
    dofile_once("mods/noita-together/files/store.lua")
    local damage_models = EntityGetFirstComponent(player_entity, "DamageModelComponent")
    if (damage_models ~= nil) then ComponentSetValue2(damage_models, "wait_for_kill_flag_on_death", true) end
    local res_x = MagicNumbersGetValue("DESIGN_PLAYER_START_POS_X")
    local res_y = MagicNumbersGetValue("DESIGN_PLAYER_START_POS_Y")
    if (not GameHasFlagRun("respawn_checkpoint_added")) then
		EntityAddComponent2(player_entity, "VariableStorageComponent", {
            name = "respawn_checkpoint",
            value_string = res_x .. "," .. res_y
        })
        GameAddFlagRun("respawn_checkpoint_added")
	end

    --if (ModSettingGet("noita-together.NT_HINTS")) then
    --    EntityLoad("mods/noita-together/files/entities/start_run_hint.xml", res_x - 45, res_y + 30)
    --end
end