if not async then
    dofile("mods/noita-together/files/scripts/coroutines.lua")
end
dofile_once("mods/noita-firefight/files/scripts/utils.lua")

--a request from another client for their own player's userId and stats
customEvents["FirefightEcho"] = function(data)
    --print("FirefightEcho")
    dofile_once("mods/noita-firefight/files/scripts/store.lua")
    dofile_once("mods/noita-together/files/scripts/json.lua")
    local roll = Random(1, #json.decode(FIREFIGHT.PlayerList))
    if roll == 1 then
        send_echo_response(data)
    end
end

--a response to a request for this client's userId and stats
customEvents["FirefightEchoResponse"] = function(data)
    --print("FirefightEchoResponse")
    dofile_once("mods/noita-firefight/files/scripts/store.lua")

    --if the attached request id doesn't match our own, then this isn't for us
    if FIREFIGHT.echo_request_id ~= data.requestId then
        return
    end

    for k, v in pairs(data) do
        print("k: " .. tostring(k) .. " | v: " .. tostring(v))
        if k == "score" then
            FIREFIGHT.score = v
        elseif k == "money" then
            FIREFIGHT.money = v
        elseif k == "userId" then
            FIREFIGHT.userId = v
        elseif k == "player_name" then
            FIREFIGHT.player_name = v
        elseif k == "round" then
            FIREFIGHT.round = v
        elseif k == "winner_name" then
            FIREFIGHT.winner_name = v
        elseif k == "phase" then
            FIREFIGHT.phase = v
        elseif k == "time_since_last_phase_start" then
            FIREFIGHT.last_phase_start = GameGetRealWorldTimeSinceStarted() - tonumber(v)
        end
    end

    --bootstrap game client into correct gme phase
    if  FIREFIGHT.phase == "post" and FIREFIGHT.winner_name ~= nil then
        FinishRun(FIREFIGHT.winner_name)
        if FIREFIGHT.winner_name ~= FIREFIGHT.player_name then
            GameSetCameraFree(true)
            local player = EntityGetWithTag("player_unit")[1]
            RecursiveSpriteDisable(player)
            local children = EntityGetAllChildren(player)
            for _, child in ipairs(children) do
                if EntityGetName(child) == "cape" then
                    EntityKill(child)
                end
            end
        end
    elseif FIREFIGHT.phase ~= "pregame" then
        _start_run = true
    end

    --wipe our id, thus ignoring all other responses to the request
    FIREFIGHT.echo_request_id = nil
end

--pre-StartRun movement info
--not used anymore cause I don't think it worked well
customEvents["FirefightPlayerMove"] = function(data)
    --print("FirefightPlayerMove")
    wsEvents["PlayerMove"](data)
end

customEvents["FirefightPlayerDeath"] = function(data)
    local player = PlayerList[data.userId]
    PlayerList[data.userId].alive = false
    PlayerList[data.userId].curHp = 0

    dofile_once("mods/noita-firefight/files/scripts/store.lua")
    dofile_once("mods/noita-together/files/scripts/json.lua")
    local firefight_player_list = json.decode(FIREFIGHT.PlayerList)
    for _, player in pairs(firefight_player_list) do
        if player.userId == data.userId then
            player.alive = false
        end
    end
    FIREFIGHT.PlayerList = json.encode(firefight_player_list)

    local msg = tostring(player.name) .. " has been eliminated"
    GamePrintImportant(msg, "")

    DespawnPlayerGhost(data.userId)
    initialize_left_statue()

    local aliveCount = 0
    local winnerName = ""

    for k, v in pairs(PlayerList) do
        if (v.curHp > 0) then
            aliveCount = aliveCount + 1
            winnerName = v.name
        end
    end

    --you just won
    if (aliveCount == 0) then
        msg = "You won!"
        GamePrintImportant(msg, "")
        NT.end_msg = msg
        FinishRun(FIREFIGHT.player_name)

    --someone else just won
    elseif (aliveCount == 1 and FIREFIGHT.alive == false) then
        local player = GetPlayer()
        if (player ~= nil) then
            damage_models = EntityGetFirstComponent(player, "DamageModelComponent")
            if (damage_models ~= nil) then
                local cur_hp = ComponentGetValue2(damage_models, "hp")
                if cur_hp == 0 then
                    msg = winnerName .. " has won"
                    GamePrintImportant(msg, "")
                    NT.end_msg = msg
                    FinishRun(winnerName)
                end
            end
        end

    --someone died but no win yet
    else
        msg = PlayerList[data.userId].name .. " has been eliminated"
        GamePrintImportant(msg, "")
    end
end

--updated stats from another player to update this client's FIREFIGHT.PlayerList
customEvents["FirefightStatUpdate"] = function(data)
    --print("FirefightStatUpdate")
    dofile_once("mods/noita-firefight/files/scripts/store.lua")
    dofile_once("mods/noita-together/files/scripts/json.lua")

    local cleaned_data = {}
    for k, v in pairs(data) do
        if k ~= "player_name" ~= "name" and k ~= "userId" then
            cleaned_data[k] = v
        end
    end
    update_player_stats(data.userId, cleaned_data)
end

--one player sending an enemy to another player, possibly this player
customEvents["FirefightSendEnemy"] = function(data)
    --print("FirefightSendEnemy")
    local from_user_id = data.userId
    local to_user_id = data.to_user
    local from_player, to_player = nil, nil

    --update the player's round_sends and notify in chat
    local player_list = get_player_list(false)
    for _, player in pairs(player_list) do
        if player.userId == from_user_id then
            from_player = player
        end
        if player.userId == to_user_id then
            to_player = player
            player.round_sends = player.round_sends + 1
            FIREFIGHT.PlayerList = json.encode(player_list)
            set_player_list(player_list)
        end
    end

    --if it was for us, spawm the enemy on us
    if to_user_id == FIREFIGHT.userId then
        --print("this enemy send is for me!")
        FIREFIGHT.round_sends = FIREFIGHT.round_sends + 1
        to_player = { player_name = FIREFIGHT.player_name }
        spawn_sent_enemy(from_player.player_name)
    end

    GamePrint(tostring(from_player.player_name) .. " sent an enemy to " .. tostring(to_player.player_name))
end

--client signals that their tinker phase is over
customEvents["FirefightReadyForCountdown"] = function(data)
    dofile_once("mods/noita-firefight/files/scripts/store.lua")
    dofile_once("mods/noita-together/files/scripts/json.lua")
    local player_list = json.decode(FIREFIGHT.PlayerList)
    for _, player in pairs(player_list) do
        if player.userId == data.userId then
            player.ready = true
        end
    end
    FIREFIGHT.PlayerList = json.encode(player_list)
end

local _AddPlayer = wsEvents["AddPlayer"]
wsEvents["AddPlayer"] = function(data)
    dofile_once("mods/noita-firefight/files/scripts/store.lua")
    dofile_once("mods/noita-together/files/scripts/json.lua")
    local player_list = json.decode(FIREFIGHT.PlayerList)
    if #player_list == 0 then
        initialize_left_statue()
    end
    if #player_list == 0 then player_list = {} end
    local firefight_data = {}
    --name -> player_name to avoid conflicting with CustomModEvents "name" property
    if data.name ~= nil then firefight_data.player_name = data.name else firefight_data.player_name = "no name" end
    if data.userId ~= nil then firefight_data.userId = data.userId else firefight_data.userId = nil end
    if data.money ~= nil then firefight_data.money = data.money else firefight_data.money = 0 end
    if data.score ~= nil then firefight_data.score = data.score else firefight_data.score = 0 end
    firefight_data.round_sends = 0
    firefight_data.alive = true
    firefight_data.ready = false
    table.insert(player_list, firefight_data)
    FIREFIGHT.PlayerList = json.encode(player_list)
    _AddPlayer(data)
end

local _RemovePlayer = wsEvents["RemovePlayer"]
wsEvents["RemovePlayer"] = function(data)
    dofile_once("mods/noita-firefight/files/scripts/store.lua")
    dofile_once("mods/noita-together/files/scripts/json.lua")
    local player_list = json.decode(FIREFIGHT.PlayerList)
    if #player_list == 0 then return end
    for index, player in pairs(player_list) do
        if player.userId == data.userId then
            player_list[index] = nil
        end
    end
    FIREFIGHT.PlayerList = json.encode(player_list)
    _RemovePlayer(data)
    initialize_left_statue()
end

local _PlayerDeath = wsEvents["PlayerDeath"]
wsEvents["PlayerDeath"] = function(data) end