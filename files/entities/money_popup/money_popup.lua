local entity_id = GetUpdatedEntityID()
local entity_x, entity_y = EntityGetTransform(entity_id)
local lua_comp = EntityGetFirstComponent(entity_id, "LuaComponent")
if lua_comp == nil then return end
local times_executed = ComponentGetValue2(lua_comp, "mTimesExecuted")

if times_executed == 0 then
    local var_comp = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent")
    if var_comp == nil then return end
    local value = ComponentGetValue2(var_comp, "value_int")
    if value == nil then return end

    local offsetx = 6
    local text = tostring(value)
    local textwidth = 0
    for i=1,#text do
        local l = string.sub( text, i, i )
        if ( l ~= "1" ) then
            textwidth = textwidth + 6
        else
            textwidth = textwidth + 3
        end
    end
    offsetx = textwidth * 0.5 - 0.5

    EntityAddComponent2(entity_id, "SpriteComponent", {
        _tags = "enabled_in_world",
        image_file = "data/fonts/font_pixel_white.xml",
        is_text_sprite = true,
        offset_x = offsetx,
        offset_y = 25,
        update_transform= true ,
        update_transform_rotation = false,
        text = tostring("+" .. value),
        z_index = -1
    })

    GameEntityPlaySound(entity_id, "bling")
else
    EntitySetTransform(entity_id, entity_x, entity_y - 0.5)
end