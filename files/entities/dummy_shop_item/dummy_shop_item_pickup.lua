dofile_once("mods/noita-firefight/files/scripts/store.lua")

function item_pickup(entity_item, entity_who_picked, item_name)
    local var_comp = EntityGetFirstComponentIncludingDisabled(entity_item, "VariableStorageComponent")
    if var_comp == nil then return end
    local actual_item = EntityLoad(ComponentGetValue2(var_comp, "value_string"))

    EntityKill(entity_item)
    GamePickUpInventoryItem(entity_who_picked, actual_item, true)

    --send money update
    local wallet_comp = EntityGetFirstComponentIncludingDisabled(entity_who_picked, "WalletComponent")
    if wallet_comp ~= nil then
        local money = ComponentGetValue2(wallet_comp, "money")
        if money ~= nil then
            local old_item_cost_comp = EntityGetFirstComponentIncludingDisabled(entity_item, "ItemCostComponent")
            local old_cost = ComponentGetValue2(old_item_cost_comp, "cost")
            local new_money = tonumber(money - old_cost)
            FIREFIGHT.money = new_money
            queue_custom_mod_event("FirefightStatUpdate", { money = new_money })
        end
    end
end

--[[function find_inventory_entity(entity_id)
    for i, child in ipairs(EntityGetAllChildren(entity_id) or {}) do
        if EntityGetName(child) == "inventory_quick" then
        return child
        end
    end
end]]