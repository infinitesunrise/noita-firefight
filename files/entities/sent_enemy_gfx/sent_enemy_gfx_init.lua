local entity_id = GetUpdatedEntityID()
local skull_sprite, from_label_sprite = nil, nil
local sprite_comps = EntityGetComponentIncludingDisabled(entity_id, "SpriteComponent")
for _, sprite in ipairs(sprite_comps) do
    if ComponentGetValue2(sprite, "image_file") == "mods/noita-firefight/files/entities/sent_enemy_gfx/sent_skull.png" then
        skull_sprite = sprite
    end
    if ComponentGetValue2(sprite, "image_file") == "data/fonts/font_pixel_white.xml" then
        from_label_sprite = sprite
    end
end
local skull_particles = EntityGetFirstComponentIncludingDisabled(entity_id, "ParticleEmitterComponent")
if skull_sprite == nil or from_label_sprite == nil or skull_particles == nil then return end

EntitySetComponentIsEnabled(entity_id, skull_sprite, true)
ComponentSetValue2(skull_particles, "is_emitting", true)
EntitySetComponentIsEnabled(entity_id, from_label_sprite, true)

local var_comps = EntityGetComponentIncludingDisabled(entity_id, "VariableStorageComponent")
for _, var_comp in ipairs(var_comps) do
    if ComponentGetValue2(var_comp, "name") == "from_name" then
        local from_name = tostring(ComponentGetValue(var_comp, "value_string"))
        local offset_x = #from_name * 1.9
        ComponentSetValue2(from_label_sprite, "offset_x", offset_x)
        ComponentSetValue2(from_label_sprite, "text", tostring(from_name))
    end
end