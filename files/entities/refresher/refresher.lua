local entity_id = GetUpdatedEntityID()
local root = EntityGetRootEntity(entity_id)

local lua_comp = EntityGetFirstComponentIncludingDisabled(entity_id, "LuaComponent")
local damage_model = EntityGetFirstComponentIncludingDisabled(root, "DamageModelComponent")
if lua_comp == nil or damage_model == nil then
     EntityKill(entity_id)
     return
end

local times_executed = ComponentGetValue2(lua_comp, "mTimesExecuted")
local x, y = EntityGetTransform(root)
--drop the heart effect on first run
if times_executed == 1 then
    local max_hp = ComponentGetValue2(damage_model, "max_hp")
    ComponentSetValue2(damage_model, "hp", max_hp)
    EntityLoad("data/entities/particles/image_emitters/heart_effect.xml", x, y - 12)
    EntityLoad("data/entities/particles/heart_out.xml", x, y - 8)
--drop the refresh effect on second run, and delete self
elseif times_executed == 2 then
    EntityLoad("data/entities/particles/image_emitters/spell_refresh_effect.xml", x, y-12)
    GameRegenItemActionsInPlayer(root)
    EntityKill(entity_id)
    return
end