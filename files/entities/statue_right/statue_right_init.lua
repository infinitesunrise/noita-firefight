dofile_once("mods/noita-firefight/files/scripts/store.lua")

local entity_id = GetUpdatedEntityID()
local item_comp = EntityGetFirstComponent(entity_id, "ItemComponent")
local item_cost_comp = EntityGetFirstComponent(entity_id, "ItemCostComponent")
local sprite_cost_comp = EntityGetFirstComponent(entity_id, "SpriteComponent", "shop_cost")
local particles_comp = EntityGetFirstComponent(entity_id, "ParticleEmitterComponent")

if GlobalsGetValue("PERK_THIS_ROUND", "0") == "1" then
    EntitySetComponentIsEnabled(entity_id, item_comp, false)
    EntitySetComponentIsEnabled(entity_id, item_cost_comp, false)
    EntitySetComponentIsEnabled(entity_id, sprite_cost_comp, false)
    ComponentSetValue2(particles_comp, "is_emitting", false)
    return
end

local cost = 50 + (FIREFIGHT.round * 50)
GlobalsSetValue("PERK_THIS_ROUND", "1")

if sprite_cost_comp ~= nil then
	local offset_x = 6
	local text = tostring(cost)
	if text ~= nil then
		local text_width = 0
		for i = 1, #text do
			local l = string.sub(text, i, i)
			if l ~= "1" then
				text_width = text_width + 6
			else
				text_width = text_width + 3
			end
		end
		offset_x = text_width * 0.5 - 0.5
		ComponentSetValue2(sprite_cost_comp, "offset_x", offset_x)
	end
end

if item_cost_comp ~= nil then
    ComponentSetValue(item_cost_comp, "cost", tostring(cost))
end

if particles_comp ~= nil then
    ComponentSetValue2(particles_comp, "is_emitting", true)
end