dofile_once("data/scripts/perks/perk.lua")
dofile_once("mods/noita-firefight/files/scripts/utils.lua")
dofile_once("mods/noita-firefight/files/scripts/store.lua")

function item_pickup(entity_item, entity_who_picked, item_name)
    --figure out what to do
    local entity_id = GetUpdatedEntityID()
    local entity_x, entity_y = EntityGetTransform(entity_id)
    local pos_x, pos_y = EntityGetTransform(entity_who_picked)
    SetRandomSeed(GameGetRealWorldTimeSinceStarted() + pos_x, GameGetFrameNum() + pos_y)
    local perk = random_perk()

    --spawn the perk and add some stuff
    local perk_entity = perk_spawn(entity_x, entity_y - 58, perk, true)

    local sprite_component = EntityGetFirstComponent(perk_entity, "SpriteComponent")
    ComponentSetValue2(sprite_component, "offset_x", 8)
    ComponentSetValue2(sprite_component, "offset_y", 16)

    toss_item(perk_entity)

    --reset the perk alter
	EntityLoad("mods/noita-firefight/files/entities/statue_right/statue_right.xml", entity_x, entity_y)
    EntityKill(entity_id)

    --send money update
    local wallet_comp = EntityGetFirstComponentIncludingDisabled(entity_who_picked, "WalletComponent")
    if wallet_comp ~= nil then
        local money = ComponentGetValue2(wallet_comp, "money")
        if money ~= nil then
            local old_item_cost_comp = EntityGetFirstComponentIncludingDisabled(entity_item, "ItemCostComponent")
            local old_cost = ComponentGetValue2(old_item_cost_comp, "cost")
            local new_money = tonumber(money - old_cost)
            FIREFIGHT.money = new_money
            queue_custom_mod_event("FirefightStatUpdate", { money = new_money })
        end
    end
end