-- yeah, I made a seven-segment digital clock in noita.
-- I should get outside more often.

-- encoding segment order is: 
-- top, topleft, topright, middle, bottomleft, bottomright, bottom
local encodings = {
    { true, true, true, false, true, true, true },      -- 0
    { false, false, true, false, false, true, false },  -- 1
    { true, false, true, true, true, false, true },     -- 2
    { true, false, true, true, false, true, true },     -- 3
    { false, true, true, true, false, true, false },    -- 4
    { true, true, false, true, false, true, true },     -- 5
    { true, true, false, true, true, true, true },      -- 6
    { true, false, true, false, false, true, false },   -- 7
    { true, true, true, true, true, true, true },       -- 8
    { true, true, true, true, false, true, false },     -- 9
    { false, false, false, false, false, false, false } -- blank
}

local entity_id = GetUpdatedEntityID()
local storage_component = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent", "input")
local input = ComponentGetValue2(storage_component, "value_string")
local digit_children = EntityGetAllChildren(entity_id)

if #input > 4 then
    input = string.sub(input, 1, 4)
end
if #input < 4 then
    input = string.rep(" ", 4 - #input) .. input
end

local numerals = {
    ["digit1"] = string.sub(input, 1, 1),
    ["digit2"] = string.sub(input, 2, 2),
    ["digit3"] = string.sub(input, 3, 3),
    ["digit4"] = string.sub(input, 4, 4)
}

for digit, numeral in pairs(numerals) do
    local encoding = encodings[11] -- blank by default
    if numeral ~= " " then
        encoding = encodings[tonumber(numeral) + 1]
    end

    local digit_id = nil
    for _, digit_child in ipairs(digit_children) do
        local name = EntityGetName(digit_child)
        if digit == name then
            digit_id = digit_child
            goto found
        end
    end
    if digit_id == nil then goto skip end --oopsie woopsie I made a fucky wucky
    ::found::

    local top = EntityGetFirstComponentIncludingDisabled(digit_id, "SpriteComponent", "top")
    local topleft = EntityGetFirstComponentIncludingDisabled(digit_id, "SpriteComponent", "topleft")
    local topright = EntityGetFirstComponentIncludingDisabled(digit_id, "SpriteComponent", "topright")
    local middle = EntityGetFirstComponentIncludingDisabled(digit_id, "SpriteComponent", "middle")
    local bottomleft = EntityGetFirstComponentIncludingDisabled(digit_id, "SpriteComponent", "bottomleft")
    local bottomright = EntityGetFirstComponentIncludingDisabled(digit_id, "SpriteComponent", "bottomright")
    local bottom = EntityGetFirstComponentIncludingDisabled(digit_id, "SpriteComponent", "bottom")

    ComponentSetValue2(top, "visible", encoding[1])
    ComponentSetValue2(topleft, "visible", encoding[2])
    ComponentSetValue2(topright, "visible", encoding[3])
    ComponentSetValue2(middle, "visible", encoding[4])
    ComponentSetValue2(bottomleft, "visible", encoding[5])
    ComponentSetValue2(bottomright, "visible", encoding[6])
    ComponentSetValue2(bottom, "visible", encoding[7])

    ::skip::
end