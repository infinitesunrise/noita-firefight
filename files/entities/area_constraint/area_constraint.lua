local entity_id = GetUpdatedEntityID()
local root = EntityGetRootEntity(entity_id)
local entity_x, entity_y = EntityGetTransform(root)

local min_x = nil
local max_x = nil
local min_y = nil
local max_y = nil

local variable_components = EntityGetComponent(entity_id, "VariableStorageComponent")
for _, comp in ipairs(variable_components) do
    local name = ComponentGetValue2(comp, "name")
    if name == "min_x" then
        min_x = ComponentGetValue2(comp, "value_int")
    elseif name == "max_x" then
        max_x = ComponentGetValue2(comp, "value_int")
    elseif name == "min_y" then
        min_y = ComponentGetValue2(comp, "value_int")
    elseif name == "max_y" then
        max_y = ComponentGetValue2(comp, "value_int")
    end
end

if entity_x < min_x or entity_x > max_x or entity_y < min_y or entity_y > max_y then
    EntitySetTransform(root,
        math.min(math.max(entity_x, min_x), max_x),
        math.min(math.max(entity_y, min_y), max_y))
end