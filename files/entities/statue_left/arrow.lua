dofile_once("mods/noita-firefight/files/scripts/utils.lua")

function item_pickup(entity_item, entity_who_picked, item_name)
    --am I the left arrow or the right arrow
    local direction = "left"
    if EntityGetName(entity_item) == "statue_left_arrow_right" then
        direction = "right"
    end
    local player_list = get_player_list(false)
    
    --get the player label entity
    local player_label_entity = nil
    local statue_left_entities = EntityGetWithTag("statue_left")
    for _, entity in ipairs(statue_left_entities) do
        if EntityGetName(entity) == "statue_left_player_label" then
            player_label_entity = entity
        end
    end
    if player_label_entity == nil then return end

    --get the purchase entity
    local purchase_entity = nil
    for _, entity in ipairs(statue_left_entities) do
        if EntityGetName(entity) == "statue_left_purchase" then
            purchase_entity = entity
        end
    end
    if purchase_entity == nil then return end

    --get current user_id
    local index = nil
    local variable_component = EntityGetFirstComponent(player_label_entity, "VariableStorageComponent")
    if variable_component == nil then return end
    index = ComponentGetValue(variable_component, "value_int")
    if index == nil then return end

    --cycle the rolodex forward or backward one index
    if direction == "left" then
        if tonumber(index) == 1 then
            index = #player_list
        else
            index = index - 1
        end
    else
        if tonumber(index) == #player_list then
            index = 1
        else
            index = index + 1
        end
    end
    local selected_player = player_list[tonumber(index)]
    local player_name = selected_player.player_name
    local player_user_id = selected_player.userId

    --update the player name label
    local sprite_component = EntityGetFirstComponent(player_label_entity, "SpriteComponent")
    if sprite_component ~= nil then
        EntityRemoveComponent(player_label_entity, sprite_component)
    end
    local offset_x = #player_name * 3 - 5
    EntityAddComponent2(player_label_entity, "SpriteComponent", {
        image_file = "data/fonts/font_pixel_white.xml",
        is_text_sprite = true,
        offset_x = offset_x,
        offset_y = 10,
        update_transform = true,
        text = player_name,
        z_index = -1
    })
    ComponentSetValue(variable_component, "value_int", index)

    --update the purchase cost label
    local cost_sprite = EntityGetFirstComponent(purchase_entity, "SpriteComponent")
    local item_cost_comp = EntityGetFirstComponent(purchase_entity, "ItemCostComponent")
    --print("cost_sprite: " .. tostring(cost_sprite))
    --print("item_cost_comp: " .. tostring(item_cost_comp))
    local cost = get_send_cost_for_player(player_user_id)
    if cost_sprite ~= nil then
        EntityRemoveComponent(purchase_entity, cost_sprite)
    end
    local offset_x = #tostring(cost) * 3 - 5
    --print("adding SpriteComponent to entity #" .. tostring(purchase_entity))
    --print("player is id #" .. tostring(EntityGetWithTag("player_unit")[1]))
    EntityAddComponent2(purchase_entity, "SpriteComponent", {
        image_file = "data/fonts/font_pixel_white.xml",
        is_text_sprite = true,
        offset_x = offset_x,
        offset_y = 6,
        update_transform = true,
        text = tostring(cost),
        z_index = -1
    })
    ComponentSetValue2(item_cost_comp, "cost", cost)

    --brand new arrow!
    if direction == "left" then
        EntityLoad("mods/noita-firefight/files/entities/statue_left/arrow_left.xml", -451, -326)
    else
        EntityLoad("mods/noita-firefight/files/entities/statue_left/arrow_right.xml", -396, -326)
    end
    EntityKill(entity_item)
end