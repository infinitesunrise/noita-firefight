dofile_once("mods/noita-firefight/files/scripts/utils.lua")
dofile_once("mods/noita-firefight/files/scripts/store.lua")

function item_pickup(entity_item, entity_who_picked, item_name)
    --get the player label entity
    local player_label_entity = nil
    local statue_left_entities = EntityGetWithTag("statue_left")
    for _, entity in ipairs(statue_left_entities) do
        if EntityGetName(entity) == "statue_left_player_label" then
            player_label_entity = entity
        end
    end
    if player_label_entity == nil then return end

    --get current user id from rolodex index
    local variable_component = EntityGetFirstComponent(player_label_entity, "VariableStorageComponent")
    if variable_component == nil then return end
    local player_index = ComponentGetValue(variable_component, "value_int")
    if player_index == nil then return end

    --make new purchase entity
    local purchase_entity = EntityLoad("mods/noita-firefight/files/entities/statue_left/statue_left_purchase.xml", -427, -321)
    local selected_player_cost = get_new_send_cost_for_player(player_index)
    local purchase_sprite_component = EntityGetFirstComponent(purchase_entity, "SpriteComponent")
    if purchase_sprite_component ~= nil then
        --[[text_width = 0
        local cost_string = tostring(selected_player_cost)
        for i = 1, cost_string do
            local l = string.sub(cost_string, i, i)
            if l ~= "1" then
                text_width = text_width + 6
            else
                text_width = text_width + 3
            end
        end
        local offset_x = -text_width * 0.5 - 0.5
        ComponentSetValue2(purchase_sprite_component, "offset_x", offset_x)]]
        ComponentSetValue2(purchase_sprite_component, "text", tostring(selected_player_cost))
    end
    local item_cost_comp = EntityGetFirstComponent(purchase_entity, "ItemCostComponent")
    if item_cost_comp ~= nil then
        ComponentSetValue2(item_cost_comp, "cost", selected_player_cost)
    end

    --send enemy event
    --send_enemy_to(player_index)
    queue_custom_mod_event(
        "FirefightSendEnemy", 
        { to_user = get_player_list(false)[tonumber(player_index)].userId }
    )

    --send player gold update
    local player = EntityGetWithTag("player_unit")[1]
    if player ~= nil then
        local wallet_comp = EntityGetFirstComponentIncludingDisabled(entity_who_picked, "WalletComponent")
        if wallet_comp ~= nil then
            local money = ComponentGetValue2(wallet_comp, "money")
            if money ~= nil then
                local old_item_cost_comp = EntityGetFirstComponentIncludingDisabled(entity_item, "ItemCostComponent")
                local old_cost = ComponentGetValue2(old_item_cost_comp, "cost")
                local new_money = tonumber(money - old_cost)
                FIREFIGHT.money = new_money
                queue_custom_mod_event("FirefightStatUpdate", { money = new_money })
            end
        end
    end

    --kill this old purchase entity
    EntityKill(entity_item)
end