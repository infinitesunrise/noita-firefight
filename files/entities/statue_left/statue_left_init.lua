dofile_once("mods/noita-firefight/files/scripts/utils.lua")

local player_list = get_player_list(false)
if player_list == nil then return end
if #player_list == 0 then return end

--calc a random-ish seed
local player_id = EntityGetWithTag("player_id")[1]
local player_x, player_y = EntityGetTransform(player_id)
SetRandomSeed(player_x, player_y)

--pick a player to start the rolodex at
local selected_player_index = Random(1, #player_list)
--print("player_list length: " .. #player_list)
--print("selected_player_index: " .. tostring(selected_player_index))
local player = player_list[tonumber(selected_player_index)]
if player == nil then return end
local selected_player_name = player.player_name
local selected_player_cost = get_send_cost_for_player(player.userId)

local statue_left_entities = EntityGetWithTag("statue_left")

--get the player label entity
local player_label_entity = nil
for _, entity in ipairs(statue_left_entities) do
    if EntityGetName(entity) == "statue_left_player_label" then
        player_label_entity = entity
    end
end

--get the purchase entity
local purchase_entity = nil
for _, entity in ipairs(statue_left_entities) do
    if EntityGetName(entity) == "statue_left_purchase" then
        purchase_entity = entity
    end
end

local text_width = 0
local offset_x = 0

--setup label entity
if player_label_entity ~= nil then
    --label: sprite text, sprite offset, variable comp
    local label_sprite_component = EntityGetFirstComponent(player_label_entity, "SpriteComponent")
    if label_sprite_component ~= nil then
        --[[text_width = 0
		for i = 1, #selected_player_name do
			local l = string.sub(selected_player_name, i, i)
			if l ~= "1" then
				text_width = text_width + 6
			else
				text_width = text_width + 3
			end
		end
		offset_x = text_width * 0.5 - 0.5]]

        offset_x = #selected_player_name * 3 - 5
        ComponentSetValue2(label_sprite_component, "text", selected_player_name)
        ComponentSetValue2(label_sprite_component, "offset_x", offset_x)
    end
    local variable_component = EntityGetFirstComponent(player_label_entity, "VariableStorageComponent")
    if variable_component ~= nil then
        ComponentSetValue2(variable_component, "value_int", selected_player_index)
    end
end

--setup purchase entity
if purchase_entity ~= nil then
    local purchase_sprite_component = EntityGetFirstComponent(purchase_entity, "SpriteComponent")
    if purchase_sprite_component ~= nil then
        text_width = 0
        local cost_string = tostring(selected_player_cost)
        for i = 1, cost_string do
            local l = string.sub(cost_string, i, i)
            if l ~= "1" then
                text_width = text_width + 6
            else
                text_width = text_width + 3
            end
        end
        offset_x = -text_width * 0.5 - 0.5
        ComponentSetValue2(purchase_sprite_component, "text", cost_string)
        --ComponentSetValue2(purchase_sprite_component, "offset_x", offset_x)
    end
    local item_cost_comp = EntityGetFirstComponent(purchase_entity, "ItemCostComponent")
    if item_cost_comp ~= nil then
        ComponentSetValue2(item_cost_comp, "cost", selected_player_cost)
    end
end