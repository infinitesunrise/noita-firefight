--try moving to a random nearby location if not in air
function material_area_checker_failed(pos_x, pos_y)
    SetRandomSeed(GameGetRealWorldTimeSinceStarted() + pos_x, GameGetFrameNum() + pos_y)
    local range_x, range_y = 64, 64
    local entity_id = GetUpdatedEntityID()
    local x, y = EntityGetTransform(entity_id)
    local new_x = x - math.floor(range_x * 0.5) + Random(1, range_x)
    local new_y = y - math.floor(range_y * 0.5) + Random(1, range_y)
    EntitySetTransform(entity_id, new_x, new_y)
end

--if in air, spawn portal and enemy
function material_area_checker_success(pos_x, pos_y)
    local entity_id = GetUpdatedEntityID()
    local x, y = EntityGetTransform(entity_id)
    local var_comp = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent")
    local file_path = ComponentGetValue2(var_comp, "value_string")

    --spawn enemy with sound effect
    GameEntityPlaySound(entity_id, "enemy_spawn")
    local enemy = EntityLoad(file_path, x, y)

    --add aggro and death effects
    EntityAddComponent2(enemy, "LuaComponent", {
        script_source_file = "mods/noita-firefight/files/scripts/firefight_enemy.lua",
        execute_on_added = true,
        remove_after_executed = true
    })
    
    --check for options and act on any found
    local var_comps = EntityGetComponentIncludingDisabled(entity_id, "VariableStorageComponent")
    for _, var_comp in ipairs(var_comps) do
        local name = ComponentGetValue2(var_comp, "name")
        --a sender name, displayed via some graphics
        if name == "from_name" then
            --display sender name and graphic
            local sent_enemy_gfx = EntityLoad(
                "mods/noita-firefight/files/entities/sent_enemy_gfx/sent_enemy_gfx.xml",
                0, 0)
            EntityAddComponent2(sent_enemy_gfx, "VariableStorageComponent", {
                name = "from_name",
                value_string = ComponentGetValue2(var_comp, "value_string")
            })
            EntityAddChild(enemy, sent_enemy_gfx)

            --double their health
            local damage_comp = EntityGetFirstComponentIncludingDisabled(enemy, "DamageModelComponent")
            if damage_comp ~= nil then
                local hp = ComponentGetValue2(damage_comp, "hp")
                if hp ~= nil then
                    ComponentSetValue2(damage_comp, "hp", hp * 2)
                end
            end
        end
    end

    --remove material checkers
    local material_checkers = EntityGetComponentIncludingDisabled(entity_id, "MaterialAreaCheckerComponent")
    for _, material_checker in ipairs(material_checkers) do
        EntityRemoveComponent(entity_id, material_checker)
    end

    --add a lifetime component
    EntityAddComponent2(entity_id, "LifetimeComponent", {
        lifetime = 60
    })

    --turn on the particles
    local particle_component = EntityGetFirstComponentIncludingDisabled(entity_id, "SpriteParticleEmitterComponent")
    ComponentSetValue2(particle_component, "is_emitting", true)

    --turn on the lights
    local light_components = EntityGetComponentIncludingDisabled(entity_id, "LightComponent")
    for _, light_comp in ipairs(light_components) do
        EntitySetComponentIsEnabled(entity_id, light_comp, true)
    end
end