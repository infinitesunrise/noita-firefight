local entity_id = GetUpdatedEntityID()
local board_x, board_y = EntityGetTransform(entity_id)
local character_width = 12
local scale = 2
local blink_frequency = 20

function off()
    local children = EntityGetAllChildren(entity_id)
    if children ~= nil then
        for _, child in ipairs(children) do
            EntityKill(child)
        end
    end
end

function scroll(message, step, width)
    local first_visible = math.ceil(((step + 2) * scale) / character_width)
    local last_visible = math.ceil(((step + width) * scale) / character_width)
    local half_board = width * scale * 0.5
    local middle = board_x + half_board

    --buffer message to fill board
    local min_message_length = math.ceil((width * scale) / character_width)
    local message_length_diff = min_message_length - #message
    if message_length_diff > 0 then
        message = string.rep(" ", message_length_diff) .. message
    end
    message = message .. " "

    for j = first_visible, last_visible, 1 do
        local i = j % #message

        local character = string.sub(message, i, i)
        if character == " " or character == "" then goto continue end
        character = match_symbols(character)

        --get the board character, or create it if it's new
        local character_name = "board_character_" .. tostring(i)
        local character_entity = EntityGetWithName(character_name)
        if character_entity == 0 then
            character_entity = EntityCreateNew(character_name)
            EntitySetTransform(
                character_entity,
                board_x + (width * scale) - 1,
                board_y)
            EntityAddComponent2(character_entity, "SpriteComponent", {
                image_file = "mods/noita-firefight/files/entities/led_board/characters_animated/" .. character .. ".xml",
                z_index = -256,
                rect_animation = "5"
            })
            EntityAddChild(entity_id, character_entity)
        end

        --move the character over one step
        local character_x = board_x
            + (character_width * (j - 1))
            - (step * scale)
            - 1
        EntitySetTransform(
            character_entity, 
            character_x,
            board_y)

        --determine animation progress
        local progress = 5
        if j == first_visible then
            local character_outside_x = math.abs(character_x - middle)
            local diff = math.abs(half_board - character_outside_x)
            progress = 5 + ((diff - 1) / 2)
        elseif j == last_visible then
            local character_outside_x = math.abs((character_x + character_width) - middle)
            local diff = math.abs(half_board - character_outside_x)
            progress = 5 - ((diff - 1) / 2)
        end

        --update rectanimation if necessary (board edge transitions)
        if j == first_visible or j == last_visible or j == (last_visible - 1) then
            local sprite_component = EntityGetFirstComponentIncludingDisabled(character_entity, "SpriteComponent")
            ComponentSetValue2(sprite_component, "rect_animation", tostring(progress))
        end

        ::continue::
    end

    local previous_first = first_visible - 1
    local previous_first_character = EntityGetWithName("board_character_" .. tostring(previous_first))
    if previous_first_character ~= nil then
        EntityKill(previous_first_character)
    end

    step = step + 1
    local max_step = #message * math.floor(character_width / scale)
    if step > max_step then step = 1 end

    return step
end

function blink(message, step, width)
    --place sprite entities
    --[[if step == 1 then
        local offset_x = character_width * #message * 0.5
        local half_board = width * scale * 0.5
        local middle = board_x + half_board
        local start_x = middle - offset_x
        
        for i = 1, #message do
            --check if character is in led board bounds
            local character_x = start_x + ((i - 1) * character_width)
            if character_x + character_width < board_x
                or character_x > board_x + (width * scale) then
                goto continue
            end

            local rect_animation = "5"
            if character_x < board_x then
                local character_outside_x = math.abs(character_x - middle)
                local diff = math.abs(half_board - character_outside_x)
                rect_animation = tostring(5 + ((diff - 1) / 2))
            end
            if character_x + character_width > board_x + (width * scale) then
                local character_outside_x = math.abs((character_x + character_width) - middle)
                local diff = math.abs(half_board - character_outside_x)
                rect_animation = tostring(5 - ((diff - 1) / 2))
            end

            --get character
            local character = string.sub(message, i, i)
            if character == " " or character == "" then goto continue end
            character = match_symbols(character)

            --add character
            local character_entity = EntityCreateNew("board_character_" .. tostring(i))
            EntitySetTransform(
                character_entity,
                character_x,
                board_y)
            EntityAddComponent2(character_entity, "SpriteComponent", {
                image_file = "mods/noita-firefight/files/entities/led_board/characters_animated/" .. character .. ".xml",
                z_index = -256,
                rect_animation = rect_animation,
                visible = true
            })
            EntityAddChild(entity_id, character_entity)

            ::continue::
        end
    end]]
    centered_message(message, width)

    --blink sprites
    if step % blink_frequency == 0 then
        local children = EntityGetAllChildren(entity_id)
        local on = math.floor(step / blink_frequency) % 2
        for _, child in ipairs(children) do
            local sprite_component = EntityGetFirstComponentIncludingDisabled(child, "SpriteComponent")
            if on == 0 then
                ComponentSetValue2(sprite_component, "visible", false)
            else
                ComponentSetValue2(sprite_component, "visible", true)
            end
        end
    end

    step = step + 1
    return step
end

function list(message, step, width)
    if step == 1 or step % 60 == 0 then
        local half_board = width * scale * 0.5
        local middle = board_x + half_board

        local children = EntityGetAllChildren(entity_id)
        if children ~= nil then
            for _, child in ipairs(children) do
                EntityKill(child)
            end
        end

        local index = 0
        if step ~= 1 then
            index = step / 60
        end

        local pages={}
        for page in string.gmatch(message, "([^|]+)") do
                table.insert(pages, page)
        end

        index = index % #pages
        local characters = pages[index + 1]
        for i = 1, #characters do
            local character = string.sub(characters, i, i)
            if character == " " or character == "" then goto continue end
            character = match_symbols(character)

            local character_x = board_x + ((i - 1) * character_width) - 1
            if character_x > board_x + (width * scale) then goto continue end
            rect_animation = "5"
            if board_x + (i * character_width) > board_x + (width * scale) then
                local character_outside_x = math.abs((character_x + character_width) - middle)
                local diff = math.abs(half_board - character_outside_x)
                rect_animation = tostring(5 - ((diff - 1) / 2))
            end

            local character_entity = EntityCreateNew("board_character_" .. tostring(i))
            EntitySetTransform(
                character_entity,
                character_x,
                board_y)
            EntityAddComponent2(character_entity, "SpriteComponent", {
                image_file = "mods/noita-firefight/files/entities/led_board/characters_animated/" .. character .. ".xml",
                z_index = -256,
                rect_animation = rect_animation,
                visible = true
            })
            EntityAddChild(entity_id, character_entity)

            ::continue::
        end
    end

    step = step + 1
    return step
end

function static(message, step, width)
    centered_message(message, width)
    step = step + 1
    return step
end

function centered_message(message, width)
    local offset_x = character_width * #message * 0.5
    local half_board = width * scale * 0.5
    local middle = board_x + half_board
    local start_x = middle - offset_x
    
    for i = 1, #message do
        --check if character is in led board bounds
        local character_x = start_x + ((i - 1) * character_width)
        if character_x + character_width < board_x
            or character_x > board_x + (width * scale) then
            goto continue
        end

        local rect_animation = "5"
        if character_x < board_x then
            local character_outside_x = math.abs(character_x - middle)
            local diff = math.abs(half_board - character_outside_x)
            rect_animation = tostring(5 + ((diff - 1) / 2))
        end
        if character_x + character_width > board_x + (width * scale) then
            local character_outside_x = math.abs((character_x + character_width) - middle)
            local diff = math.abs(half_board - character_outside_x)
            rect_animation = tostring(5 - ((diff - 1) / 2))
        end

        --get character
        local character = string.sub(message, i, i)
        if character == " " or character == "" then goto continue end
        character = match_symbols(character)

        --add character
        local character_entity = EntityCreateNew("board_character_" .. tostring(i))
        EntitySetTransform(
            character_entity,
            character_x,
            board_y)
        EntityAddComponent2(character_entity, "SpriteComponent", {
            image_file = "mods/noita-firefight/files/entities/led_board/characters_animated/" .. character .. ".xml",
            z_index = -256,
            rect_animation = rect_animation,
            visible = true
        })
        EntityAddChild(entity_id, character_entity)

        ::continue::
    end
end

function match_symbols(character)
    if character == ":" then character = "_colon" end
    if character == "." then character = "_period" end
    if character == "!" then character = "_exclamation" end
    if character == "?" then character = "_questionmark" end
    if character == "-" then character = "_dash" end
    if character == "_" then character = "_underscore" end
    if character == "@" then character = "_at" end
    if character == "/" then character = "_forwardslash" end
    if character == "'" or character == "`" then character = "_apostraphe" end
    if character == "," then character = "_comma" end
    return character
end