dofile_once("mods/noita-firefight/files/scripts/utils.lua")

local entity_id = GetUpdatedEntityID()

local message_component = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent", "message")
local routine_component = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent", "routine")
local width_component = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent", "width")
local step_component = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent", "step")
local reset_component = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent", "reset")

if message_component == nil 
    or width_component == nil 
    or step_component == nil 
    or routine_component == nil
    or reset_component == nil
    then return end

local message = ComponentGetValue2(message_component, "value_string")
local routine = ComponentGetValue2(routine_component, "value_int")
local width = ComponentGetValue2(width_component, "value_int")
local step = ComponentGetValue2(step_component, "value_int")
local reset = ComponentGetValue2(reset_component, "value_bool")

if message == nil 
    or width == nil 
    or step == nil 
    or routine == nil
    or reset == nil
    then return end

--handle reset for new message
if reset == true then
    step = 1
    local children = EntityGetAllChildren(entity_id)
    if children ~= nil then
        for _, child in ipairs(children) do
            EntityKill(child)
        end
    end
    ComponentSetValue2(reset_component, "value_bool", false)
end

dofile("mods/noita-firefight/files/entities/led_board/board_functions.lua")

if routine == LEDRoutine.OFF then
    off()
elseif routine == LEDRoutine.SCROLL then
    step = scroll(message, step, width)
elseif routine == LEDRoutine.BLINK then
    step = blink(message, step, width)
elseif routine == LEDRoutine.LIST then
    step = list(message, step, width)
elseif routine == LEDRoutine.STATIC then
    step = static(message, step, width)
end

ComponentSetValue2(step_component, "value_int", step)