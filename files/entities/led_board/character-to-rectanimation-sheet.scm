; gimp 'script-fu' script to turn font characters into rectanimation sprite sheets

(define (character-to-rectanimation-sheet image)
  (let* ( (baselayer 0) (newfloating 0) (newlayer 0) )
    (gimp-undo-push-group-start image)
    (gimp-edit-copy-visible image)
    (gimp-image-resize image 108 14 49 1)

    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-resize-to-image-size baselayer)
    (set! newfloating (car (gimp-edit-paste baselayer FALSE)))
    (gimp-floating-sel-to-layer newfloating)
    (set! newlayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-translate newlayer 12 0)
    (gimp-image-merge-down image newlayer 0)

    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-resize-to-image-size baselayer)
    (set! newfloating (car (gimp-edit-paste baselayer FALSE)))
    (gimp-floating-sel-to-layer newfloating)
    (set! newlayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-translate newlayer 24 0)
    (gimp-image-merge-down image newlayer 0)

    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-resize-to-image-size baselayer)
    (set! newfloating (car (gimp-edit-paste baselayer FALSE)))
    (gimp-floating-sel-to-layer newfloating)
    (set! newlayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-translate newlayer 36 0)
    (gimp-image-merge-down image newlayer 0)

    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-resize-to-image-size baselayer)
    (set! newfloating (car (gimp-edit-paste baselayer FALSE)))
    (gimp-floating-sel-to-layer newfloating)
    (set! newlayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-translate newlayer 48 0)
    (gimp-image-merge-down image newlayer 0)

    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-resize-to-image-size baselayer)
    (set! newfloating (car (gimp-edit-paste baselayer FALSE)))
    (gimp-floating-sel-to-layer newfloating)
    (set! newlayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-translate newlayer -12 0)
    (gimp-image-merge-down image newlayer 0)

    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-resize-to-image-size baselayer)
    (set! newfloating (car (gimp-edit-paste baselayer FALSE)))
    (gimp-floating-sel-to-layer newfloating)
    (set! newlayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-translate newlayer -24 0)
    (gimp-image-merge-down image newlayer 0)

    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-resize-to-image-size baselayer)
    (set! newfloating (car (gimp-edit-paste baselayer FALSE)))
    (gimp-floating-sel-to-layer newfloating)
    (set! newlayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-translate newlayer -36 0)
    (gimp-image-merge-down image newlayer 0)

    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-resize-to-image-size baselayer)
    (set! newfloating (car (gimp-edit-paste baselayer FALSE)))
    (gimp-floating-sel-to-layer newfloating)
    (set! newlayer (car (gimp-image-get-active-layer image)))
    (gimp-layer-translate newlayer -48 0)
    (gimp-image-merge-down image newlayer 0)

    (gimp-rect-select image 3 0 8 14 0 FALSE 0)
    (gimp-rect-select image 17 0 6 14 0 FALSE 0)
    (gimp-rect-select image 31 0 4 14 0 FALSE 0)
    (gimp-rect-select image 45 0 2 14 0 FALSE 0)
    (gimp-rect-select image 61 0 2 14 0 FALSE 0)
    (gimp-rect-select image 73 0 4 14 0 FALSE 0)
    (gimp-rect-select image 85 0 6 14 0 FALSE 0)
    (gimp-rect-select image 97 0 8 14 0 FALSE 0)
    
    (set! baselayer (car (gimp-image-get-active-layer image)))
    (gimp-edit-clear baselayer)

    (gimp-undo-push-group-end image)
  )
)

(script-fu-register
  "character-to-rectanimation-sheet"
  "RectAnimation"
  "Image to RectAnimation"
  "infinitesunrise"
  "infinitesunrise"
  "February 2023"
  "RGB*, GRAY*"
  SF-IMAGE    "Image" 0
)
(script-fu-menu-register "character-to-rectanimation-sheet" "<Image>/RectAnimation")