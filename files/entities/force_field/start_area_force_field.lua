local entity_id = GetUpdatedEntityID()

local mode = nil
local previous_mode = nil
local previous_comp = nil

local variable_storage_components = EntityGetComponentIncludingDisabled(entity_id, "VariableStorageComponent")
for _, comp in ipairs(variable_storage_components) do
    local name = ComponentGetValue2(comp, "name")
    if name == "mode" then
        mode = ComponentGetValue2(comp, "value_string")
    end
    if name == "previous_mode" then
        previous_mode = ComponentGetValue2(comp, "value_string")
        previous_comp = comp
    end
end

if mode ~= previous_mode then
    local force_field_left = nil
    local force_field_right = nil

    local children = EntityGetAllChildren(entity_id)
    for _, child in ipairs(children) do
        local name = EntityGetName(child)
        if name == "force_field_left" then force_field_left = child end
        if name == "force_field_right" then force_field_right = child end
    end

    local emitter_left = EntityGetFirstComponentIncludingDisabled(force_field_left, "ParticleEmitterComponent")
    local emitter_right = EntityGetFirstComponentIncludingDisabled(force_field_right, "ParticleEmitterComponent")
    local sprite_left = EntityGetFirstComponentIncludingDisabled(force_field_left, "SpriteComponent")
    local sprite_right = EntityGetFirstComponentIncludingDisabled(force_field_right, "SpriteComponent")

    if mode == "on" then
        ComponentSetValue2(emitter_left, "is_emitting", true)
        ComponentSetValue2(emitter_right, "is_emitting", true)
        ComponentSetValue2(sprite_left, "visible", true)
        ComponentSetValue2(sprite_right, "visible", true)
    elseif mode == "off" then
        ComponentSetValue2(emitter_left, "is_emitting", false)
        ComponentSetValue2(emitter_right, "is_emitting", false)
        ComponentSetValue2(sprite_left, "visible", false)
        ComponentSetValue2(sprite_right, "visible", false)
    elseif mode == "left" then
        ComponentSetValue2(emitter_left, "is_emitting", true)
        ComponentSetValue2(emitter_right, "is_emitting", false)
        ComponentSetValue2(sprite_left, "visible", true)
        ComponentSetValue2(sprite_right, "visible", false)
    elseif mode == "right" then
        ComponentSetValue2(emitter_left, "is_emitting", false)
        ComponentSetValue2(emitter_right, "is_emitting", false)
        ComponentSetValue2(sprite_left, "visible", true)
        ComponentSetValue2(sprite_right, "visible", true)
    end

    ComponentSetValue2(previous_comp, "value_string", mode)
end