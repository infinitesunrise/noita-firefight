local entity = GetUpdatedEntityID()
local combo, combo_cooldown, previous_enemy_kills = nil, nil, nil
local combo_comp, cooldown_comp, prev_kills_comp = nil, nil, nil

local var_comps = EntityGetComponentIncludingDisabled(entity, "VariableStorageComponent")
for _, var_comp in ipairs(var_comps) do
    local name = ComponentGetValue2(var_comp, "name")
    if name == "combo" then
        combo = ComponentGetValue2(var_comp, "value_int")
        combo_comp = var_comp
    elseif name == "combo_cooldown" then
        combo_cooldown = ComponentGetValue2(var_comp, "value_int")
        cooldown_comp = var_comp
    elseif name == "previous_enemy_kills" then
        previous_enemy_kills = ComponentGetValue2(var_comp, "value_int")
        prev_kills_comp = var_comp
    end
end

local enemies_killed = tonumber(StatsGetValue("enemies_killed"))

--print("enemies_killed: " .. tostring(enemies_killed))
--print("previous_enemy_kills: " .. tostring(previous_enemy_kills))

if tonumber(enemies_killed) ~= tonumber(previous_enemy_kills) then
    local new_kills = enemies_killed - previous_enemy_kills
    local new_combo = combo + new_kills
    local announcement = nil
    
    if combo < 10 and new_combo >= 10 then
        announcement = "announcer_10x_kill"
    elseif combo < 9 and new_combo >= 9 then
        announcement = "announcer_9x_kill"
    elseif combo < 8 and new_combo >= 8 then
        announcement = "announcer_18x_kill"
    elseif combo < 7 and new_combo >= 7 then
        announcement = "announcer_7x_kill"
    elseif combo < 6 and new_combo >= 6 then
        announcement = "announcer_6x_kill"
    elseif combo < 5 and new_combo >= 5 then
        announcement = "announcer_5x_kill"
    elseif combo < 4 and new_combo >= 4 then
        announcement = "announcer_4x_kill"
    elseif combo < 3 and new_combo >= 3 then
        announcement = "announcer_3x_kill"
    elseif combo < 2 and new_combo >= 2 then
        announcement = "announcer_2x_kill"
    end

    --print("combo: " .. tostring(combo))
    --print("new_combo: " .. tostring(new_combo))
    --print("announcement: " .. tostring(announcement))

    if announcement ~= nil then
        --local entity_x, entity_y = EntityGetTransform(entity)
        --GamePlaySound(
        --    "mods/noita-firefight/files/audio/noita_firefight.snd",
        --    announcement,
        --    entity_x, entity_y
        --)
        print("playing " .. announcement)
        GameEntityPlaySound(entity, announcement)
    end

    combo = new_combo
    combo_cooldown = 121
    ComponentSetValue2(combo_comp, "value_int", combo)
    ComponentSetValue2(cooldown_comp, "value_int", combo_cooldown)
    ComponentSetValue2(prev_kills_comp, "value_int", enemies_killed)
end

if combo_cooldown > 0 then
    combo_cooldown = combo_cooldown - 1
    ComponentSetValue2(cooldown_comp, "value_int", combo_cooldown)
end

if combo_cooldown == 0 and combo ~= 0 then
    ComponentSetValue2(combo_comp, "value_int", 0)
end