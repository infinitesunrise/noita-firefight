dofile_once("mods/noita-firefight/files/scripts/utils.lua")

function death(damage_type_bit_field, damage_message, entity_thats_responsible, drop_items)
    local player = EntityGetWithTag("player_unit")[1]
    local local_player = get_local_player()
    local greed_multiplier = 1 + GameGetGameEffectCount(player, "EXTRA_MONEY")

    --instant money collection
    local new_money = 0
    for _, gold_nugget in ipairs(EntityGetWithTag("gold_nugget")) do
        local var_comps = EntityGetComponentIncludingDisabled(gold_nugget, "VariableStorageComponent") or {}
        for _, var_comp in ipairs(var_comps) do
            local name = ComponentGetValue2(var_comp, "name")
            if name == "gold_value" then
                local value = ComponentGetValue2(var_comp, "value_int") * greed_multiplier
                new_money = new_money + value
            end
        end
        EntityKill(gold_nugget)
    end
    if new_money > 0 then
        local total_money = nil
        local wallet_comp = EntityGetFirstComponentIncludingDisabled(player, "WalletComponent")
        if wallet_comp ~= nil then
            local money = ComponentGetValue2(wallet_comp, "money")
            if money ~= nil then
                total_money = tonumber(money + new_money)
                ComponentSetValue2(wallet_comp, "money", total_money)
            end
        end

        local total_score = local_player.score + new_money

        if total_score ~= nil and total_money ~= nil then
            update_local_player_stats({
                money = total_money,
                score = total_score
            })
            queue_custom_mod_event("FirefightStatUpdate", { money=total_money, score=total_score })
        end
    end

    local x, y = EntityGetTransform(GetUpdatedEntityID())

    --drop loot
    SetRandomSeed(x, y)
    roll_random_loot(x, y)

    --money popup
    local money_popup = EntityLoad("mods/noita-firefight/files/entities/money_popup/money_popup.xml", x, y)
    local variable_comp = EntityGetFirstComponentIncludingDisabled(money_popup, "VariableStorageComponent")
    if variable_comp ~= nil then
        ComponentSetValue2(variable_comp, "value_int", new_money)
    end
end