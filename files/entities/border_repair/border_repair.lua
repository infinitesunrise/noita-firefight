dofile_once("mods/noita-firefight/files/scripts/fancy_pixels.lua")

function repair_border_for_step(step)
    local x, y = -768, -768
    if step < 6 then
        x = x + (256 * step)
    elseif step < 9 then
        x = 512
        y = y + (256 * (step - 5))
    elseif step < 14 then
        x = 512 - (256 * (step - 9))
        y = 0
    elseif step < 16 then
        y = (step - 13) * 256
    else
        kill_self()
    end

    --[[
        the 256 x 256 border repair png is actually 266 x 266
        with a 5-pixel transparent border (hence "buffered" in png file name).
        this pixel scene gets drawn -5,-5 from the inteded location,
        in order to get world gen to draw pretty 5px edges where it meets the play space
    ]]
    fancy_pixels(
        "mods/noita-firefight/data/biome_impl/pixel_scenes/",
        "firefight_border_repair_buffered.png",
        x - 5, y - 5,
        "",
        "",
        nil,
        nil,
        0)
end

function kill_self()
    entity_id = GetUpdatedEntityID()
    EntityKill(entity_id)
end

local entity_id = GetUpdatedEntityID()
local var_comp = EntityGetFirstComponentIncludingDisabled(entity_id, "VariableStorageComponent")
if var_comp == nil then kill_self() end
local step = ComponentGetValue2(var_comp, "value_int")
if step == nil then kill_self() end
repair_border_for_step(step)
ComponentSetValue2(var_comp, "value_int", tonumber(step + 1))