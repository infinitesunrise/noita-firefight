dofile_once("data/scripts/gun/procedural/gun_procedural.lua")
dofile_once("mods/noita-firefight/files/scripts/store.lua")

local cost = FIREFIGHT.round * 10
local level = 2 + math.floor(FIREFIGHT.round / 3)
generate_empty_gun_and_throw(cost, level)