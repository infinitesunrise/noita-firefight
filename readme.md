# Noita Firefight
![map preview](files/readme/map_preview.png)

## What's this?
Noita Firefight is a multiplayer PvPvE survival mode addon for [Noita Together.](https://github.com/soler91/noita-together) Players fight through increasingly difficult waves of enemies on a small map, collecting wands and spells from kills. Altars around the map sell random perks, items, and enemy drops against other players. The last Noita standing wins!

### Installation
1. If you haven't already done so, install and set up the Noita Together desktop client and mod according to Soler's instructions on [the Noita Together wiki.](https://github.com/soler91/noita-together/wiki)

2. Download this repo to a new folder in your Noita mods directory name it `noita-firefight`.

3. Ensure that both Noita Firefight and Noita Together are enabled in the game's Mods menu, and that Firefight is listed *before* Together.
![correct mod order](files/readme/mod_order.png)

4. Join a Noita Together lobby intended for Firefight, and start a new game!

### Instructions for Lobby Hosts

* Ensure that all players are running the same version of the noita-firefight mod by checking the mod name in the game lobby.
![how to check mod version](files/readme/lobby_mod_version.png)

* Turn off all NT options (Or leave some on, I'm not the boss of you), change the seed, and Apply settings.
![suggested lobby settings](files/readme/lobby_settings.png)

* Hit 'Start Game' in the lobby to begin gameplay. Play can continue until there is only one player left standing, but the lobby will not reset until you set a new seed, at which point players should start a new game from the Noita game menu.

* At the start of the first game in a lobby, players will not be able to see one another until you hit Start Run. At the start of subsequent games in the same lobby, players will be able to see one another before the game begins.

### Some known bugs, workin on em

* Players might get the wrong Twitch username assigned to themselves in the player list and game announcements.

* Players might fall out of sync with the gameplay loop and be SOL.

* Please report reprodicible / repeatable bugs! Also, pull requests are welcome.